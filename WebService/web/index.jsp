<%-- 
    Document   : index
    Created on : May 4, 2015, 4:18:55 PM
    Author     : iaw46993441
--%>
<%@ page import="java.io.*,java.util.*,java.sql.*" %>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <sql:setDataSource var="usuari" driver="org.postgresql.Driver"
                           url="jdbc:postgresql://localhost/hospital"
                           user="postgres"  password="postgres"/>

        <sql:query dataSource="${usuari}" var="result">
            SELECT * from login;
        </sql:query>

        <table border="1" width="100%">
            <tr>
                <th>Usuari Username</th>
                <th>Usuari Contrasenya</th>
            </tr>
            <c:forEach var="row" items="${result.rows}">
                <tr>
                    <td><c:out value="${row.username}"/></td>
                    <td><c:out value="${row.password}"/></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
