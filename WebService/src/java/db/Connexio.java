package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexio {

    private static Connexio instance = null;
    public static final String URL = "jdbc:postgresql://localhost/hospital";
    public static final String USER = "postgres";
    public static final String PASSWORD = "postgres";
    public static final String DRIVER_CLASS = "org.postgresql.Driver";

    private Connexio() {
        try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connexio getInstance() {
        if (instance == null) {
            instance = new Connexio();
        }
        return instance;
    }

    public static Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println("ERROR: Unable to Connect to Database.");
        }
        return connection;
    }

}
