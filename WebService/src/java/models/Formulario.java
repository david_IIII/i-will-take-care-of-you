/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import db.Connexio;
import db.DbUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import operaciones.WebServices;

/**
 *
 * @author iaw38873251
 */
public class Formulario {
    //atributos de la clase
    Timestamp data;
    int num_historial;
    double tension_arterial_max;
    double tension_arterial_min;
    int frequencia_cardiaca;
    double temperatura;
    int diuresis;
    int azucar;
    String descripcion;
    List <Foto> listaFoto;
    private Connection connection;
    private Statement statement;
    
    /**
     * Constructor por defecto
     */ 
    public Formulario (){
        this.diuresis = 0;
        this.azucar = 0;
        this.descripcion = null;
        listaFoto = new ArrayList();
    }
    
    /**
     * Constructor 
     * @param data
     * @param num_historial
     * @param tension_arterial_max
     * @param tension_arterial_min
     * @param frequencia_cardiaca
     * @param temperatura
     * @param diuresis
     * @param azucar
     * @param descripcion 
     */
    public Formulario(Timestamp data, int num_historial, double tension_arterial_max, double tension_arterial_min, int frequencia_cardiaca, double temperatura, int diuresis, int azucar, String descripcion) {
        this.data = data;
        this.num_historial = num_historial;
        this.tension_arterial_max = tension_arterial_max;
        this.tension_arterial_min = tension_arterial_min;
        this.frequencia_cardiaca = frequencia_cardiaca;
        this.temperatura = temperatura;
        this.diuresis = diuresis;
        this.azucar = azucar;
        this.descripcion = descripcion;
        this.listaFoto = new ArrayList();
    }
    
    /**
     *  Funcion que inserta en la tabla formulario los datos introducidos por el usuario
     * @return true si no ha error false si se ha producido un error.
     */
    public Boolean insertarFormulario(){
        String insert = "INSERT INTO FORMULARI_CONTROL_ENFERMERA VALUES ('"+data+"',"+num_historial+","+ tension_arterial_max + "," + tension_arterial_min + "," + frequencia_cardiaca + ","+ temperatura+ ","+ diuresis + "," + azucar + ",'" + descripcion + "')";
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            statement.executeUpdate(insert);
            return insertINFORMETOFORM();
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
    }
    
    /**
     * Funcion que inserta en la tabla informetoform la relacion entre el informe y el formulario
     * @return true si no ha error false si se ha producido un error.
     */
    public boolean insertINFORMETOFORM(){
        String select = "SELECT * FROM informe WHERE abierto and num_historial = " + num_historial ;
        ResultSet resultado;
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                int id = resultado.getInt("id");
                String insert = "INSERT INTO informetoform VALUES ("+id+",'"+data+"',"+num_historial+")";
                statement.executeUpdate(insert);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }
    
    /**
     * Funcion que recopila todas las fotos relacionadas con el formulario
     * @return true si no ha error false si se ha producido un error.
     */
    public boolean recopilarFotos() {
        String select = "SELECT * FROM foto WHERE data = '"+data+"' and num_historial = " + num_historial ;
        ResultSet resultado;
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                int id = resultado.getInt("id");
                String path = resultado.getString("path");
                
                Foto foto = new Foto();
                foto.setId(id);
                foto.setPath(path);
                foto.setData(data);
                foto.setNum_historial(num_historial);
                
                listaFoto.add(foto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }
     
    //getters & setters

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public List<Foto> getListaFoto() {
        return listaFoto;
    }

    public void setListaFoto(List<Foto> listaFoto) {
        this.listaFoto = listaFoto;
    }

    public double getTension_arterial_max() {
        return tension_arterial_max;
    }

    public void setTension_arterial_max(double tension_arterial_max) {
        this.tension_arterial_max = tension_arterial_max;
    }

    public double getTension_arterial_min() {
        return tension_arterial_min;
    }

    public void setTension_arterial_min(double tension_arterial_min) {
        this.tension_arterial_min = tension_arterial_min;
    }

    public int getFrequencia_cardiaca() {
        return frequencia_cardiaca;
    }

    public void setFrequencia_cardiaca(int frequencia_cardiaca) {
        this.frequencia_cardiaca = frequencia_cardiaca;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public int getDiuresis() {
        return diuresis;
    }

    public void setDiuresis(int diuresis) {
        this.diuresis = diuresis;
    }

    public int getAzucar() {
        return azucar;
    }

    public void setAzucar(int azucar) {
        this.azucar = azucar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Data=" + data + ";Numero historial=" + num_historial + ";Tensión arterial maxima=" + tension_arterial_max + ";Tensión arterial minima=" + tension_arterial_min + ";Frequencia cardiaca=" + frequencia_cardiaca + ";Temperatura=" + temperatura + ";Diuresis=" + diuresis + ";Azucar=" + azucar + ";Descripción=" + descripcion ;
    }     
}
