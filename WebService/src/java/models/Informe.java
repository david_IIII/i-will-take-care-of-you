package models;

import db.Connexio;
import db.DbUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import operaciones.WebServices;

/**
 *
 * @author iaw38873251
 */
public class Informe {
    //atributos de la clase
    //numero de historial del paciente 
    int num_historial;
    int id;
    //patologia o especialidad a la que pertenece el informe
    String patologia;
    String descripcion;
    //el informe estara abierto siempre y cuando el el paciente esté siguiendo un tratamiento
    Boolean abierto;
    //Tratamiento que recibe el paciente
    Tratamiento tratamiento;
    //lista de formularios que pertenece al informe
    List <Formulario> listaForm;
    //lista de formularios que pertenece al informe
    List <Prueba> listaPruebas;
    
    
    
    private Connection connection;
    private Statement statement;
    
    /**
     * Constructor por defector
     */ 
    public Informe(){
        this.listaForm = new ArrayList();
        this.listaPruebas = new ArrayList();
    }
    
   /**
    * Constructor
    * 
    * @param num_historial : número de historial del paciente al que pertenece
    * @param id : identificador del número de informe
    * @param patologia :  especialidad a que corresponde el informe
    * @param descripcion : explicación o comentario de algun dato a destacar 
    */ 
    public Informe(int num_historial, int id, String patologia, String descripcion) {
        this.num_historial = num_historial;
        this.id = id;
        this.patologia = patologia;
        this.descripcion = descripcion;
        this.listaForm = new ArrayList();
        this.listaPruebas = new ArrayList();
    }
    
    /**
     * Funcion que consulta los informes abiertos del paciente
     * @return lista de informes
     */
    public List consultarInforme(){
        String select = "SELECT * FROM informe WHERE abierto and num_historial = " + num_historial ;
        ResultSet resultado;
        Informe info;
        List<Informe> listaInform = new ArrayList();
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                int id1 = resultado.getInt("id");
                String patologia1 = resultado.getString("patologia");
                String descripcion1 = resultado.getString("descripcion");
                info = new Informe(this.num_historial,id1,patologia1,descripcion1);
                listaInform.add(info);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return listaInform;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaInform;
    }
    
    /**
     * Funcion que recopila los formularios del informe especificado
     * @return 
     */
    public List recopilarFormularios(){
        String select = "SELECT * FROM informetoform i,FORMULARI_CONTROL_ENFERMERA f  WHERE i.num_historial = f.num_historial and i.data = f.data and i.num_historial = " + num_historial + " and  informe_id = " +id;
        ResultSet resultado;
        Formulario form;
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                Timestamp data = resultado.getTimestamp("data");
                double tensMax = resultado.getDouble("tension_arterial_max");
                double tensMin = resultado.getDouble("tension_arterial_min");
                int frecCard = resultado.getInt("frequencia_cardiaca");
                double temperatura = resultado.getDouble("temperatura");
                int diuresis = resultado.getInt("diuresis");
                int azucar = resultado.getInt("azucar");
                String descripcion1 = resultado.getString("descripcion");

                form = new Formulario();
                form.setNum_historial(num_historial);
                form.setData(data);
                form.setTension_arterial_max(tensMax);
                form.setTension_arterial_min(tensMin);
                form.setFrequencia_cardiaca(frecCard);
                form.setTemperatura(temperatura);
                form.setDiuresis(diuresis);
                form.setAzucar(azucar);
                form.setDescripcion(descripcion1);
                form.recopilarFotos();

                listaForm.add(form);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return listaForm;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaForm;
    }
    
    /**
     * Funcion que recopila todos los informes del paciente, tanto abiertos como cerrados
     * @return lista de informes
     */
    public List consultarHistorial(){
        String select = "SELECT * FROM informe WHERE num_historial = " + num_historial ;
        ResultSet resultado;
        Informe info;
        List<Informe> listaInform = new ArrayList();
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                int id1 = resultado.getInt("id");
                String patologia1 = resultado.getString("patologia");
                String descripcion1 = resultado.getString("descripcion");
                info = new Informe(this.num_historial,id1,patologia1,descripcion1);
                listaInform.add(info);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return listaInform;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaInform;
    }
    
    /**
     * Funcion que muestra los tratamientos del informe selecionado
     * @return 
     */
    public List mostrarTratamientos() {
        //creamos la query para la consulta de la tabla formulario
        String select = "SELECT * FROM TRATAMIENTO t, INFORME i WHERE i.id = t.informe_id and i.num_historial = t.num_historial and i.num_historial = " + num_historial + " and fecha_fin is null";
        //variable resultado que va guardando los datos 
        ResultSet resultado = null;
        List<Informe> listInfo = new ArrayList<>();
        try {
            //hacemos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            //mientras devuelva algun dato la consulta
            while (resultado.next()) {
                Tratamiento trat = new Tratamiento();
                trat.setDescripcion(resultado.getString(4));
                trat.setNum_historial(num_historial);
                trat.setFecha_inicio(resultado.getTimestamp(1));
                trat.setInforme_id(resultado.getInt(3));
                Informe info = new Informe();
                info.setId(resultado.getInt(7));
                info.setNum_historial(num_historial);
                info.setTratamiento(trat);
                info.setDescripcion(resultado.getString(9));
                info.setPatologia(resultado.getString(8));
                listInfo.add(info);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return listInfo;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listInfo;
    }
    
    /**
     * Funcion que recopila todas las pruebas del paciente
     * @return 
     */
    public List recopilarPruebas(){
        String select = "SELECT * FROM PRUEBAS  WHERE num_historial = "+num_historial+" and informe_id = " +id;
        ResultSet resultado;
        Prueba prueba;
        try {
            //creamos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                Timestamp data = resultado.getTimestamp("data");
                int idPrueba = resultado.getInt("id");
                String tipo = resultado.getString("tipo");
                String centro = resultado.getString("centro");
                String descripcion1 = resultado.getString("descripcion");

                prueba = new Prueba();
                prueba.setNum_historial(num_historial);
                prueba.setInforme_id(id);
                prueba.setId(idPrueba);
                prueba.setTipo(tipo);
                prueba.setData(data);
                prueba.setCentro(centro);
                prueba.setDescripcion(descripcion1);
                
                listaPruebas.add(prueba);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return listaPruebas;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaPruebas;
    }
    
    //getters & setters

    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatologia() {
        return patologia;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getAbierto() {
        return abierto;
    }

    public void setAbierto(Boolean abierto) {
        this.abierto = abierto;
    }

    public List<Formulario> getListaForm() {
        return listaForm;
    }

    public void setListaForm(List<Formulario> listaForm) {
        this.listaForm = listaForm;
    }
    
    public Tratamiento getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(Tratamiento tratamiento) {
        this.tratamiento = tratamiento;
    }
    
    @Override
    public String toString() {
        return "Número historial=" + num_historial + ";Id=" + id + ";Patología=" + patologia + ";Descripción=" + descripcion+ ";" + tratamiento;
    }
}
