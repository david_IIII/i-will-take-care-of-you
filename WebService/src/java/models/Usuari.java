package models;

import db.Connexio;
import db.DbUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import operaciones.WebServices;

public class Usuari {

    String username;
    String contrasenya;
    int id;
    private Connection connection;
    private Statement statement;
    
    /**
     * Constructor por defecto
     */
    public Usuari() {
    }
    
    /**
     * Constructor
     * @param username
     * @param contrasenya
     * @param id 
     */
    public Usuari(String username, String contrasenya,int id) {
        this.username = username;
        this.contrasenya = contrasenya;
        this.id = id;
    }

    /**
     * Funcion que valida al usuario con los usuarios registrados en la base de datos
     * @return devuelve el cargo del usuario
     */
    public String validarUsuari(){
        String query = "SELECT * from login l, personal p where l.personal_id = p.id and  username = '" + username + "' and password = '" + contrasenya + "'";
        ResultSet resultado;
        try {
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(query);
            if (!resultado.next()) {
                return "";
            } else {
                return resultado.getString("cargo");
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
    }
    // getters & setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Usuari: " + username + ";Contrasenya: " + contrasenya
                + ";Id: "+id;
    }
}
