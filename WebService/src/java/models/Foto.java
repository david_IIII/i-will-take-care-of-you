package models;

import db.Connexio;
import db.DbUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import operaciones.WebServices;

/**
 *
 * @author iaw38873251
 */
public class Foto {

    //atributos de la clase
    int id;
    String path;
    int num_historial;
    Timestamp data;

    private Connection connection;
    private Statement statement;

    /**
     * Constructor por defecto
     */
    public Foto() {

    }

    /**
     * Constructor
     * @param id: el numero identificador de la fotografía
     * @param path: el path donde esta guradada la fotografia
     * @param num_historial: numero del historial del paciente a quien corresponde la foto
     * @param data: fecha y hora en que se ha enviado el formulario al que pertenece la foto
     */
    public Foto(int id, String path, int num_historial, Timestamp data) {
        this.id = id;
        this.path = path;
        this.num_historial = num_historial;
        this.data = data;
    }

    /**
     * Metodo para insertar foto en la base de datos
     * @return 
     */
    public boolean insertarFoto() {
        String insert = "INSERT INTO FOTO VALUES (DEFAULT,'" + path + "'," + num_historial + ",'" + data + "')";
        try {
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            statement.executeUpdate(insert);
            return true;
        //tratamos los possibles errores   
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return false;

    }
    
    //getters & setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Id foto::" + id + ",Número historial::" + num_historial + ",Path::"+ path + ",Data::" + data;
    }
}
