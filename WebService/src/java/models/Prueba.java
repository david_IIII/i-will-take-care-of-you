/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Timestamp;

/**
 *
 * @author iaw46993441
 */
public class Prueba {
    int num_historial;
    int informe_id;
    int id;
    String tipo;
    Timestamp data;
    String centro;
    String descripcion;
    
    /**
     * Constructor per defecte
     */
    public Prueba() {
    }
    
    /**
     *  Constructor
     * 
     * @param num_historial
     * @param informe_id
     * @param id
     * @param tipo
     * @param data
     * @param centro
     * @param descripcion 
     */
    public Prueba(int num_historial, int informe_id, int id, String tipo, Timestamp data, String centro, String descripcion) {
        this.num_historial = num_historial;
        this.informe_id = informe_id;
        this.id = id;
        this.tipo = tipo;
        this.data = data;
        this.centro = centro;
        this.descripcion = descripcion;
    }

    //Getters and Setters
    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public int getInforme_id() {
        return informe_id;
    }

    public void setInforme_id(int informe_id) {
        this.informe_id = informe_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Número historial=" + num_historial + ";Id informe=" + informe_id + ";Id=" + id + ";Tipo=" + tipo + ";Data=" + data + ";Centro=" + centro + ";Descripcion=" + descripcion;
    }
}
