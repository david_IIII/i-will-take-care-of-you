package models;

import db.Connexio;
import db.DbUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import operaciones.WebServices;

/**
 *
 * @author iaw38873251
 */
public class Tratamiento {

    //atributos de la clase

    Timestamp fecha_inicio;
    int num_historial;
    int informe_id;
    String descripcion;
    Timestamp fecha_final;

    private Connection connection;
    private Statement statement;

    /**
     * constructor por defecto
     */
    public Tratamiento() {

    }

    /**
     * Constructor
     * @param fecha_inicio: la fecha de inicio del tratamiento
     * @param num_historial: el numero de historial del paciente a que se le aplica el tratamiento
     * @param informe_id: identificador del informe a que pertenece el tratamiento 
     * @param descripcion: descripcion del tratamiento que se tiene que seguir
     */
    public Tratamiento(Timestamp fecha_inicio, int num_historial, int informe_id, String descripcion) {
        this.fecha_inicio = fecha_inicio;
        this.num_historial = num_historial;
        this.informe_id = informe_id;
        this.descripcion = descripcion;
    }

    /**
     * Funcion que consulta le descripcion del Tratamiento que queremos consultar
     * @return 
     */
    public String consultarDescripcion() {
        //creamos la query para la consulta de la tabla formulario
        String select = "SELECT * FROM TRATAMIENTO WHERE informe_id = " + informe_id + " and num_historial = " + num_historial + " and fecha_fin is null";
        //variable resultado que va guardando los datos 
        ResultSet resultado = null;
        try {
            //hacemos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            //mientras devuelva algun dato la consulta
            if (resultado.next()) {
                return resultado.getString("descripcion");
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return "error al comprobar la base de datos!";
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return "no hay tratamiento abierto!";
    }
    
    /**
     * Funcion que inserta en la tabla el tratamiento y actualiza la fecha final del tratamiento anterior
     * @return 
     */
    public boolean insertarTratamiento(){
        // creamos la query para introducir en la tabla tratamiento
        // previa actualizacion de la tabla
        String insert = "INSERT INTO TRATAMIENTO VALUES(current_timestamp,"+ num_historial +","+informe_id +",'"+descripcion+"',null)";
        String update = "UPDATE TRATAMIENTO SET fecha_fin = current_timestamp where num_historial = "+num_historial +" and informe_id = "+informe_id +" and fecha_fin is null";
        try {
            //hacemos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            statement.executeUpdate(update);
            statement.executeUpdate(insert);
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }

    //getters & setters
    public Timestamp getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Timestamp fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public int getInforme_id() {
        return informe_id;
    }

    public void setInforme_id(int informe_id) {
        this.informe_id = informe_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(Timestamp fecha_final) {
        this.fecha_final = fecha_final;
    }

    @Override
    public String toString() {
        return "Fecha inicio=" + fecha_inicio + ";Numero historial=" + num_historial + ";Id informe=" + informe_id + ";Descripción=" + descripcion + ";Fecha final=" + fecha_final;
    }
}
