/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author iaw38873251
 */
public class Paciente {
   //atributos de la clase
    //numero de la seguridad social del paciente
    String cip;
    //codigo paciente
    int id;
    String nombre;
    String apellidos;
    boolean genero;
    String fecha_nacimiento;
    int altura;
    int peso;
    int telefono;
    String estado_civil;
    //numero de historial
    int num_historial;
    int direccion_id;
    //lista de informes del paciente
    List <Informe> listaInforme;
    
    //constructor
    public Paciente(){
    }
    
    public Paciente(String cip, String nombre,String apellidos,String fecha_nacimiento,int num_historial){
        this.cip = cip;
        this.nombre =  nombre;
        this.apellidos = apellidos;
        this.fecha_nacimiento = fecha_nacimiento;
        this.num_historial = num_historial;
    }

    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public boolean getGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }

    public int getNum_historial() {
        return num_historial;
    }

    public void setNum_historial(int num_historial) {
        this.num_historial = num_historial;
    }

    public int getDireccion_id() {
        return direccion_id;
    }

    public void setDireccion_id(int direccion_id) {
        this.direccion_id = direccion_id;
    }

    public List<Informe> getListaInforme() {
        return listaInforme;
    }

    public void setListaInforme(List<Informe> listaInforme) {
        this.listaInforme = listaInforme;
    }
    

    @Override
    public String toString() {
        //return "Paciente:" + "CIP=" + cip + ";Id=" + id + ";Nombre=" + nombre + ";Apellidos=" + apellidos + ";Genero=" + genero + ";Fecha_nacimiento=" + fecha_nacimiento + ";Altura=" + altura + "Peso=" + peso + ", telefono=" + telefono + ", estado_civil=" + estado_civil + ", num_historial=" + num_historial + ", direccion_id=" + direccion_id + '}';
        String retorna = "CIP=" + cip + ";Nombre=" + nombre + ";Apellidos=" + apellidos + ";Genero=";
        retorna += (genero)? "Home" : "Dona" ;
        retorna += ";Fecha_nacimiento=" + fecha_nacimiento +";num_historial=" + num_historial;
        return retorna;
    }

    
    
    
    
}
