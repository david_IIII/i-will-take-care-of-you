/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import db.Connexio;
import db.DbUtil;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

import models.Formulario;
import models.Informe;
import models.Usuari;
import models.Paciente;
import models.Prueba;
import models.Tratamiento;
import models.Foto;

/**
 *
 * @author iaw46993441
 */
@WebService(serviceName = "WebServices")
public class WebServices {

    private Connection connection;
    private Statement statement;

    /**
     * Web service operation
     *
     * @param user
     * @param passwd
     * @return
     */
    @WebMethod(operationName = "validaUsuari")
    public String validaUsuari(@WebParam(name = "user") String user, @WebParam(name = "passwd") String passwd) {
       Usuari usuari = new Usuari();
       usuari.setUsername(user);
       usuari.setContrasenya(passwd);
       return usuari.validarUsuari();
    }

    /**
     * Web service operation
     *
     * @param tensionMaxima
     * @param tensionMinima
     * @param frequencia
     * @param descripcion
     * @param diuresis
     * @param temperatura
     * @param azucar
     * @param num_historial
     * @return
     */
    @WebMethod(operationName = "insertarFormulari")
    public String insertarFormulari(@WebParam(name = "tensionMaxima") double tensionMaxima, @WebParam(name = "tensionMinima") double tensionMinima, @WebParam(name = "frequencia") int frequencia, @WebParam(name = "temperatura") double temperatura, @WebParam(name = "diuresis") int diuresis, @WebParam(name = "azucar") int azucar, @WebParam(name = "descripcion") String descripcion, @WebParam(name = "num_historial") int num_historial) {
        Formulario form = new Formulario();
        Timestamp data = new Timestamp(new Date().getTime());
        form.setData(data);
        form.setNum_historial(num_historial);
        form.setTension_arterial_max(tensionMaxima);
        form.setTension_arterial_min(tensionMinima);
        form.setTemperatura(temperatura);
        form.setFrequencia_cardiaca(frequencia);
        form.setAzucar(azucar);
        form.setDiuresis(diuresis);
        form.setDescripcion(descripcion);
        Boolean fet = form.insertarFormulario();
        return data.toString();
    }

    /**
     * Web service operation
     *
     * @param nombre
     * @param apellidos
     * @return
     */
    @WebMethod(operationName = "consultaPacient")
    public String consultaPacient(@WebParam(name = "nombre") String nombre, @WebParam(name = "apellidos") String apellidos) {
        //creem la query per la consulta dels pacients amb el nom i cognoms entrats per parametre
        String select = "SELECT * FROM PACIENTE where nombre = '" + nombre + "' and apellidos = '" + apellidos + "'";
        //creem la llista de pacients on guardarem la resposta del select enviada a la base de dades
        List<Paciente> listaPaciente = new ArrayList<>();
        Paciente paciente = null;
        ResultSet resultado = null;
        try {
            //fem la conexio a la base de dades i executem ala query
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                //creem un pacient
                paciente = new Paciente();
                //guardem els valors a cada atribut que necessitem per mostrar la consulta
                paciente.setNombre(resultado.getString("nombre"));
                paciente.setApellidos(resultado.getString("apellidos"));
                paciente.setCip(resultado.getString("cip"));
                paciente.setFecha_nacimiento(resultado.getString("fecha_nacimiento"));
                paciente.setNum_historial(resultado.getInt("num_historial"));
                paciente.setGenero(resultado.getBoolean("genero"));
                //afegim el pacient a la llista
                listaPaciente.add(paciente);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            //resposta = ex.getMessage();
            return "error!";
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaPaciente.toString();
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @return
     */
    @WebMethod(operationName = "consultarInformes")
    public String consultarInformes(@WebParam(name = "num_historial") int num_historial) {
        Informe infor = new Informe();
        infor.setNum_historial(num_historial);
        List<Informe> fet = infor.consultarInforme();
        return fet.toString();
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @param id
     * @return
     */
    @WebMethod(operationName = "mostrarInforme")
    public String mostrarInforme(@WebParam(name = "num_historial") int num_historial, @WebParam(name = "id") int id) {
        Informe infor = new Informe();
        infor.setNum_historial(num_historial);
        infor.setId(id);
        List<Formulario> fet = infor.recopilarFormularios();
        List<Prueba> fet2 = infor.recopilarPruebas();
        String formFoto = "";
        for(Formulario form : fet){
            formFoto += form.toString() + ";Foto=" + form.getListaFoto().toString()+",";
        }
        
        return formFoto + "," + fet2.toString();
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @return
     */
    @WebMethod(operationName = "mostrarHistorial")
    public String mostrarHistorial(@WebParam(name = "num_historial") int num_historial) {
        Informe infor = new Informe();
        infor.setNum_historial(num_historial);
        //return form.insertINFORMETOFORM();
        List<Informe> fet = infor.consultarHistorial();
        return fet.toString();
    }

    /**
     * Web service operation
     *
     * @return
     */
    @WebMethod(operationName = "consultaGrafics")
    public String consultaGrafics() {
        //creem la query per la consulta dels pacients amb el nom i cognoms entrats per parametre
        String select = "select * from informetoform i, formulari_control_enfermera f, informe inf where i.data=f.data and i.num_historial=f.num_historial and i.informe_id=inf.id and inf.num_historial=f.num_historial and inf.abierto";
        //creem la llista de pacients on guardarem la resposta del select enviada a la base de dades
        List<Formulario> listaForm = new ArrayList<>();
        Formulario form = null;
        ResultSet resultado = null;
        try {
            //fem la conexio a la base de dades i executem ala query
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            while (resultado.next()) {
                //creem un pacient
                form = new Formulario();
                //guardem els valors a cada atribut que necessitem per mostrar la consulta
                form.setTension_arterial_max(resultado.getDouble("tension_arterial_max"));
                form.setTension_arterial_min(resultado.getDouble("tension_arterial_min"));
                form.setFrequencia_cardiaca(resultado.getInt("frequencia_cardiaca"));
                form.setTemperatura(resultado.getDouble("temperatura"));
                form.setData(resultado.getTimestamp("data"));
                //afegim el pacient a la llista
                listaForm.add(form);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return "error!";
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return listaForm.toString();
    }

    /**
     * Web service operation
     *
     * @return
     */
    @WebMethod(operationName = "consultaConstantes")
    public String consultaConstantes() {
        //creamos la query para la consulta de la tabla formulario
        String select = "SELECT * FROM FORMULARI_CONTROL_ENFERMERA WHERE data = (select max(data) from formulari_control_enfermera)";
        //variable resultado que va guardando los datos 
        ResultSet resultado = null;
        //instanciamos un formulario
        Formulario form = null;

        try {
            //hacemos la conexion a la base de datos
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            resultado = statement.executeQuery(select);
            //mientras devuelva algun dato la consulta
            while (resultado.next()) {
                form = new Formulario();
                //guardem els valors a cada atribut que necessitem per mostrar la consulta
                form.setData(resultado.getTimestamp("data"));
                form.setNum_historial(resultado.getInt("num_historiaL"));
                form.setTension_arterial_max(resultado.getFloat("tension_arterial_max"));
                form.setTension_arterial_min(resultado.getFloat("tension_arterial_min"));
                form.setFrequencia_cardiaca(resultado.getInt("frequencia_cardiaca"));
                form.setTemperatura(resultado.getFloat("temperatura"));
                form.setDiuresis(resultado.getInt("diuresis"));
                form.setAzucar(resultado.getInt("azucar"));
                form.setDescripcion(resultado.getString("descripcion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            return "error!";
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return form.toString();
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @param id
     * @return
     */
    @WebMethod(operationName = "consultarTratamiento")
    public String consultarTratamiento(@WebParam(name = "num_historial") int num_historial, @WebParam(name = "id") int id) {
        Tratamiento trat = new Tratamiento();
        trat.setNum_historial(num_historial);
        trat.setInforme_id(id);
        String fet = trat.consultarDescripcion();
        return fet;
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @return
     */
    @WebMethod(operationName = "mostrarTratamiento")
    public String mostrarTratamiento(@WebParam(name = "num_historial") int num_historial) {
        Informe info = new Informe();
        info.setNum_historial(num_historial);
        List<Informe> fet = info.mostrarTratamientos();
        return fet.toString();
    }

    /**
     * Web service operation
     *
     * @param num_historial
     * @param id
     * @param descripcion
     * @return
     */
    @WebMethod(operationName = "insertarTratamientos")
    public boolean insertarTratamientos(@WebParam(name = "num_historial") int num_historial, @WebParam(name = "id") int id, @WebParam(name = "descripcion") String descripcion) {
        Tratamiento trata = new Tratamiento();
        trata.setNum_historial(num_historial);
        trata.setInforme_id(id);
        trata.setDescripcion(descripcion);
        return trata.insertarTratamiento();
    }
    
    /**
     * Web service operation
     *
     * @param num_historial
     * @param data
     * @param path
     * @return
     */
    @WebMethod(operationName = "insertarFoto")
    public boolean insertarFoto(@WebParam(name = "num_historial") int num_historial,@WebParam(name="data") String data, @WebParam(name = "path") String path) {
        Timestamp fecha = Timestamp.valueOf(data);
        Foto foto = new Foto();
        foto.setNum_historial(num_historial);
        foto.setData(fecha);
        foto.setPath(path);
        
        return foto.insertarFoto();
    }
}
