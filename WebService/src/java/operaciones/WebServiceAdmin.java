/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import db.Connexio;
import db.DbUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import models.Usuari;

/**
 *
 * @author iaw46993441
 */
@WebService(serviceName = "WebServiceAdmin")
public class WebServiceAdmin {

    private Connection connection;
    private Statement statement;
    
    /**
     * Web service operation
     * @param usuari
     * @param contrasenya
     * @param identificador
     * @return 
     */
    @WebMethod(operationName = "altaUsuari")
    public Boolean altaUsuari(@WebParam(name = "usuari") String usuari, @WebParam(name = "contrasenya") String contrasenya, @WebParam(name = "identificador") int identificador) {
        String query = "INSERT INTO login VALUES('"+usuari+"','"+contrasenya+"',"+ identificador +")";
        try {
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }

    /**
     * Web service operation
     * @param usuari
     * @param contrasenya
     * @return 
     */
    @WebMethod(operationName = "baixaUsuari")
    public Boolean baixaUsuari(@WebParam(name = "usuari") String usuari, @WebParam(name = "contrasenya") String contrasenya) {
        //TODO write your implementation code here:
        
        String queryselect = "SELECT * FROM login WHERE username = '"+usuari+"' AND password = '"+contrasenya+"'";
        String query = "DELETE FROM login WHERE username = '"+usuari+"' AND password = '"+contrasenya+"'";
        try {
            
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(queryselect);
            if( resultado.next() ){
                statement.executeUpdate(query);
            } else {
                return false;
            }
        } catch  ( SQLException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }

    /**
     * Web service operation
     * @return 
     */
    @WebMethod(operationName = "consultaContrasenyes")
    public String consultaContrasenyes() {
        //TODO write your implementation code here:
        String query = "SELECT * FROM login";
        List<Usuari> list = new ArrayList<>();
        Usuari usuari = null;
        ResultSet rs = null;
        try {
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()) {
                usuari = new Usuari();
                usuari.setUsername(rs.getString("username"));
                usuari.setContrasenya(rs.getString("password"));
                usuari.setId(rs.getInt("personal_id"));
 
                list.add(usuari);
            }
        } catch (SQLException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            DbUtil.close(rs);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return list.toString();
    }

    /**
     * Web service operation
     * @param usuari
     * @param contrasenya
     * @return 
     */
    @WebMethod(operationName = "modificaUsuari")
    public Boolean modificaUsuari(@WebParam(name = "usuari") String usuari, @WebParam(name = "contrasenya") String contrasenya) {
        
        String queryselect = "SELECT * FROM login WHERE username = '"+usuari+"'";
        String query = "UPDATE login SET password = '"+contrasenya+"' WHERE username = '"+usuari+"'";
        try {
            connection = Connexio.getInstance().createConnection();
            statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(queryselect);
            if( resultado.next() ){
                statement.executeUpdate(query);
            } else {
                return false;
            }
        } catch  ( SQLException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return true;
    }
}
