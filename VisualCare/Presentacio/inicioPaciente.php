<?php
session_start();
// si el usuario no se ha accedido anteriormente se le redirige a la pagina de login
if( ! isset($_SESSION['usuari'])){
    header('location:../');
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Paciente</title>
        <link href='../lib/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' >
        <link href='../lib/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' >
        <script src='../lib/fullcalendar-2.3.1/lib/moment.min.js'></script>
        <script src='../lib/fullcalendar-2.3.1/lib/jquery.min.js'></script>
        <script src='../lib/fullcalendar-2.3.1/fullcalendar.min.js'></script>
        <script src="../lib/Chart.js-master/Chart.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <!-- JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css" >
        <link rel="stylesheet" type="text/css" href="../css/pagPrincipal.css" >
        <link rel="stylesheet" type="text/css" href="../css/inicioPaciente.css" >
        <script type="text/javascript">
            var usuari = '<?php echo $_SESSION['usuari']; ?>';
        </script>
        <script type="text/javascript" src="../js/inicioPaciente.js"></script>
    </head>
    <body>
        <nav class="navSuperior panel-heading navbar navbar-fixed-top" >
            <div class="todoCabeza">
                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-4" ><img src="../img/logo_vallhebron1.png" width="100px" height="55px" alt="Logo del hospital Vall d'Hebron de Barcelona"></div>
                <div class="loginUser col-md-3 col-sm-3 col-lg-3 col-xs-4">
                    <div class="usuario" ><b><?php echo $_SESSION['usuari']; ?></b></div>
                    <a href="../Negoci/cerrarSession.php" class="desconect  btn-danger" aria-label="Left Align" title="Boton para salir de la sesión" accesskey="d">
                        <span class="desc glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div> 
            </div>
        </nav>
        <!--tabla informativa sobre los datos del paciente seleccionado-->
        <div class="datosPaciente" >
            <table summary="Tabla que contiene datos personales del paciente. El nombre, cip, fecha de nacimiento y apellidos." class="tablaDatos" dir="ltr">
                <tr class="fila">
                    <td id="nombre_paciente" class="colum TNombre">Nombre:</td>
                    <td headers="nombre_paciente" class="colum resultado"><?php echo $_SESSION['nombre']; ?></td>
                    <td id="cip_paciente" class="colum TCip"><abbr title="Numero de la seguridad social">CIP:</abbr></td>
                    <td headers="cip_paciente" class="colum resultado"><?php echo $_SESSION['cip']; ?></td>
                </tr>
                <tr class="fila">
                    <td id="apellidos_paciente" class="colum TPaciente">Apellidos:</td>
                    <td headers="apellidos_paciente" class="colum resultado"><?php echo $_SESSION['apellidos']; ?></td>
                    <td id="fecha_nacimiento_paciente" class="colum TEdad">Fecha nacimiento:</td>
                    <td headers="decha_naciento_paciente" class="colum resultado"><?php echo $_SESSION['fecha_nacimiento']; ?></td>
                </tr>
            </table>
        </div>
        <!--muestra de las diferentes operaciones que puede realizar el usario en forma de botones-->
        <div id="mostrarInfo" ></div>
        <div class="container-fluid cuerpo">
            <div class="operaciones col-sm-6">
                <button id="formulario" class="form btn btn-primary btn-lg " type="button"  aria-expanded="false" onclick="<?php if($_SESSION['userProfesion'] === 'Medico') echo 'mostrarTratamiento()'; else echo 'mostrar()'; ?>" onkeypress="<?php if($_SESSION['userProfesion'] === 'Medico') echo 'mostrarTratamiento()'; else echo 'mostrar()'; ?>">
                    <span class="iconForm glyphicon glyphicon-pencil" aria-hidden="true" ></span><p><?php if($_SESSION['userProfesion'] === 'Medico') echo 'MODIFICAR TRATAMIENTO'; else echo 'RELLENAR FORMULARIO'; ?></p>
                </button>
                <button  id="informe" class="form btn btn-primary btn-lg " type="button"  aria-expanded="false" onclick="informe()" onkeypress="informe()">
                    <span class="iconInf glyphicon glyphicon-book" aria-hidden="true"></span><p>CONSULTAR INFORME</p>
                </button>
                <button id="historial" class="form btn btn-primary btn-lg " type="button"  aria-expanded="false" onclick="historial()" onkeypress="historial()">
                    <span class="iconHist glyphicon glyphicon-list-alt" aria-hidden="true"></span><p>CONSULTAR HISTORIAL</p>
                </button>
                <button id="historial" class="form btn btn-primary btn-lg " type="button"  aria-expanded="false" onclick="paciente()"  onkeypress="paciente()">
                    <span class="iconHist glyphicon glyphicon-user" aria-hidden="true"></span><p>CAMBIAR PACIENTE</p>
                </button>
            </div>
            <div class="col-sm-6">
                <!--muestra el grafico evolutivo del paciente segun sus constantes-->
                <canvas id="grafic"></canvas>
                <!--muestra los diferentes tratamientos que esta siguiendo el paciente-->
                <div class="tratamiento"></div>
            </div>
        </div>
    </body>
</html>