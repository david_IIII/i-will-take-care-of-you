<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Fer una foto</title>
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="../js/mainFoto.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/fotos.css">
        <link rel="stylesheet" type="text/css" href="../css/pagPrincipal.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
        <nav class="navSuperior panel-heading navbar navbar-fixed-top" >
            <div class="todoCabeza">
                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-4" ><img src="../img/logo_vallhebron1.png" width="100px" height="55px" alt="Logo del hospital Vall d'Hebron de Barcelona"></div>
                <div class="loginUser col-md-3 col-sm-3 col-lg-3 col-xs-4">
                    <div class="usuario"><b><?php echo $_SESSION['usuari']; ?></b></div>
                    <a href="../Negoci/cerrarSession.php" class="desconect  btn-danger" aria-label="Left Align" title="Boton para salir de la sesión" accesskey="d">
                        <span class="desc glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div> 
            </div>
        </nav>
        <!--tabla informativa sobre los datos del paciente seleccionado-->
        <div class="datosPaciente" >
            <table summary="Tabla que contiene datos personales del paciente. El nombre, cip, fecha de nacimiento y apellidos." class="tablaDatos" dir="ltr">
                <tr class="fila">
                    <td id="nombre_paciente" class="colum TNombre">Nombre:</td>
                    <td headers="nombre_paciente" class="colum resultado"><?php echo $_SESSION['nombre']; ?></td>
                    <td id="cip_paciente" class="colum TCip"><abbr title="Numero de la seguridad social">CIP:</abbr></td>
                    <td headers="cip_paciente" class="colum resultado"><?php echo $_SESSION['cip']; ?></td>
                </tr>
                <tr class="fila">
                    <td id="apellidos_paciente" class="colum TPaciente">Apellidos:</td>
                    <td headers="apellidos_paciente" class="colum resultado"><?php echo $_SESSION['apellidos']; ?></td>
                    <td id="fecha_nacimiento_paciente" class="colum TEdad">Fecha nacimiento:</td>
                    <td headers="decha_naciento_paciente" class="colum resultado"><?php echo $_SESSION['fecha_nacimiento']; ?></td>
                </tr>
            </table>
        </div>
        <div class="container">
            <!-- boton para volver a la pagina del paciente -->
            <button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button" aria-expanded="false"  onclick="location.href='inicioPaciente.php'"></button>
            <div class="foto">
                <!-- video para capturar la imagen. -->
                <video id="screenshot-stream" class="videostream" autoplay=""></video>
                <!-- donde se mantiene la imagen para guardar -->
                <canvas id="screenshot-canvas"></canvas>
                <p>
                    <button id="screenshot-button" class="btn-primary btn-sm" type="button" aria-expanded="false">Capturar</button>
                    <button id="screenshot-enviar-button" class="btn-primary btn-sm" type="button" aria-expanded="false">Enviar</button>
                </p>
            </div>
        </div>
    </body>
</html>