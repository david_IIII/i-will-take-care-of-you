<?php
session_start();
// si el usuario no se ha accedido anteriormente se le redirige a la pagina de login
if (!isset($_SESSION['usuari'])) {
    header('location:../');
}
// se inicia la cookie de las camas
if (!isset($_COOKIE['cama'])) {
    $cama = [];
    //array de 20 0.
    for ($i = 0; $i < 20; $i++) {
        array_push($cama, 0);
    }
    setcookie('cama', serialize($cama), time() + 60 * 60 * 24 * 7, '/');
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Página Principal</title>
        <link href='../lib/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' type="text/css" />
        <link href='../lib/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' type="text/css" >
        <script src='../lib/fullcalendar-2.3.1/lib/moment.min.js'></script>
        <script src='../lib/fullcalendar-2.3.1/lib/jquery.min.js'></script>
        <script src='../lib/fullcalendar-2.3.1/fullcalendar.js'></script>
        <script src='../lib/fullcalendar-2.3.1/lang/es.js'></script>
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css" >
        <link rel="stylesheet" type="text/css" href="../css/pagPrincipal.css" >
        <script type="text/javascript">
            var usuari = '<?php echo $_SESSION['usuari']; ?>';
            var cama = <?php
            $camas = (isset($_COOKIE['cama'])) ? unserialize($_COOKIE['cama']) : $cama;
            $array = "[" . $camas[0];
            for ($i = 1; $i < count($camas); $i++) {
                $array .= "," . $camas[$i];
            }
            $array .= "]";
            echo $array;
            ?>;
        </script>
        <script src='../js/principal.js'></script>
    </head>
    <body>
        <nav class="navSuperior panel-heading navbar navbar-fixed-top" >
            <div class="todoCabeza">
                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-4" ><img src="../img/logo_vallhebron1.png" width="100px" height="55px" alt="Logo del Hospital de la Vall d'Hebron de barcelona"></div>
                <div class="loginUser col-md-3 col-sm-3 col-lg-3 col-xs-4">
                    <div class="usuario" ><b><?php echo $_SESSION['usuari']; ?></b></div>
                    <a href="../Negoci/cerrarSession.php" class="desconect  btn-danger" aria-label="Left Align" title="Boton para salir de la sesión" accesskey="d">
                        <span class="desc glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>  
            </div>
        </nav>
        <!--muestra en forma de tabla los posibles pacientes-->
        <form id="tablaConsultaPaciente" name="tablaConsultaPaciente"  action="../Presentacio/inicioPaciente.php" method="post">
            <div >
                <?php
                //tabla que aparece con la informacion de los pacientes con los datos introducidos en el formulario de datos paciente
                if (isset($_SESSION['consultar']) && $_SESSION['consultar'] !== "[]") {
                    $cambiar = array("[", "]");
                    $consultar = $_SESSION['consultar'];
                    $consultar = str_replace($cambiar, "", $consultar);
                    $consultar = explode(',', $consultar);
                    $primer = explode(";", $consultar[0]);

                    echo
                    '<table class="tablaPaciente" summary="Tabla que muestra a los pacientes con nombre y apellidos iguales a los introduciodos en el formulario.">
			<thead>
                            <tr>';
                    for ($i = 0; $i < (count($primer)); $i++) {
                        $aux = explode('=', $primer[$i]);
                        echo '<th>' . $aux[0] . '</th>';
                    }
                    echo '<th>&nbsp</th>';
                    echo '</tr>
                        </thead>
			<tbody>';
                    for ($i = 0; $i < count($consultar); $i++) {
                        echo '<tr>';
                        $primer = explode(";", $consultar[$i]);
                        for ($j = 0; $j < count($primer); $j++) {
                            $aux = explode('=', $primer[$j]);
                            echo '<td id="' . $aux[0] . $i . '">' . $aux[1] . '</td>';
                        }
                        echo '<td><button type="submit" id="confirmar' . $i . '" name="confirmar" value="confirmar" class="confirmar btn btn-success btn-sm col-md-3 col-sm-4 col-lg-3 col-xs-6 " aria-expanded="false" role="confirmar"><span  class="icon glyphicon glyphicon-ok" aria-hidden="true"></span></button></td></tr>';
                    }
                    echo '</tbody>
                    </table>';
                }
                ?>
            </div>
        </form>
        <div class="datosCentral col-md-3 col-sm-3 col-lg-2 col-xs-5" >
            <!-- formulario para introducir los datos del paciente-->
            <form id="formPaciente" name="formPaciente" class="box loginPaciente col-md-3 col-sm-3 col-lg-3 col-xs-5" role="form" action="../Negoci/consultarPaciente.php" method="post" >
                <h3 class="error"><?php print_r($_SESSION['error']); ?></h3>
                <legend class="cabecera">
                    <p class="titol1">datos paciente</p>
                </legend>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nombre" id="nombre" name="nombre" title="Nombre del paciente" tabindex="1">
                    <input type="text" class="form-control" placeholder="Apellidos" id="apellido1" name="apellidos" title="Apellidos del paciente" tabindex="2">
                    <!-- Posible mejora introduccion del QR -->
                    <!--<header class="cabecera" >
                        <p class="titol2">Acceso QR</p>
                    </header>
                    <input type="file" accept="image/*" id="capture" capture="camera">-->
                    <button type="submit" id="consultar" name="consultar" value="consultar" class="confirmar btn btn-success btn-sm col-md-3 col-sm-4 col-lg-3 col-xs-6 " aria-expanded="false"  >
                        <span class="icon glyphicon glyphicon-ok" aria-hidden="true"></span>
                    </button>
                </div>
            </form>
            <!--muestra un calendario que permite al usuario editar y organizar sus tareas de forma dinámica-->
            <div class="calendario col-md-3 col-sm-3 col-lg-3 col-xs-5"></div>
            <!--muestra un grafico editable con el que el usuario puede marcar de forma dinámica las habitaciones que estan ocupadas y hacer un control de ocupación que hay en todo momento en planta-->
            <div class="camas col-md-3 col-sm-3 col-lg-3 col-xs-5">
                <h2 class="titol3" >Habitaciones</h2>
                <!--script que muestra un gráfico en forma de casillas en las que el usuario puede marcar con el raton de forma dinámica las habitaciones que están ocupadas en ese momento
                de esta forma puede hacer un control de la ocupación de pacientes que hay en la planta-->
                <?php
                $html = "";
                $cama = unserialize($_COOKIE['cama']);
                for ($i = 0; $i < 20; $i++) {
                    $html .= '<div id="' . ($i + 1) . '" class="bed';
                    $html .= ($cama[$i] == 1) ? " redBed" : "";
                    $html .= '" ><span class="iconoBed glyphicon glyphicon-bed" aria-hidden="true"><p class="bajo">' . ($i + 1) . '</p></span></div>';
                }
                echo $html;
                ?>
            </div>
        </div>
    </body>
</html>
<?php 
//la consulta se pone a null para que no te aparezcan los datos despues de recargar la pagina.
if (isset($_SESSION['consultar'])) {
    $_SESSION['consultar']= null;
}
?>
