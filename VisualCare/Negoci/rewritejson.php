<?php
//recuperamos los valores del archivo con el tipo de dato json para modificarlos
$jsonData = file_get_contents("../js/calendar_" . $_POST['user'] . ".json");
//decodificamos el json a objeto para tratarlo en php
$phpData = json_decode($jsonData, true);
if (isset($_POST['eliminar'])) {
    $event = json_decode($_POST['evento']);
    foreach ($phpData as $key=>$value) {
        if (is_numeric($event->_id) && $value['id'] == $event->_id) {
            unset($phpData[$key]);
        } else if($value['title'] === $event->title ) {
            $start = split('T',$value['start']);
            $start2 = split(':', $start[1]);
            $startEvent = split('T',$event->start);
            $startEvent2 = split('\.', $startEvent[1]);
            $startEvent3 = split(':', $startEvent2[0]);
            if($startEvent2[0] === "00:00:00" && $start[0] == $startEvent[0]){
                unset($phpData[$key]);
            } else if($startEvent2[0] !== "00:00:00" && $start2[1] == $startEvent3[1] && $start2[2] == $startEvent3[2] ){
                unset($phpData[$key]);
            }
        }
    }
} else if(isset ($_POST['insertar'])){
    $valors = [];
    $keys = [];
    if (isset($_POST['title'])){
        array_push($keys , "title");
        array_push($valors , $_POST['title']);
    }
    if (isset($_POST['start'])){
        array_push($keys , "start");
        array_push($valors , $_POST['start']);
    }
    
    $valor = array_combine($keys, $valors );
    print_r($valor);
    array_push($phpData, $valor);   
}

//guarda los valores modificados reescribiendo el archivo json
file_put_contents('../js/calendar_' . $_POST['user'] . '.json', json_encode($phpData));
