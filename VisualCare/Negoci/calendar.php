<?php
header("Content-Type: application/json");

require dirname(__FILE__) . '/utils.php';
// Parse the timezone parameter if it is present.

$timezone = new DateTimeZone('Europe/Madrid');


// Read and parse our events JSON file into an array of event data arrays.
$jsonData = file_get_contents("../js/calendar_".$_POST['user'].".json");
$input_arrays = json_decode($jsonData, true);

// Accumulate an output array of event data arrays.
$output_arrays = array();
foreach ($input_arrays as $array) {
    // Convert the input array into a useful Event object
    $event = new Event($array, $timezone);
    $output_arrays[] = $event->toArray();
}

// Send JSON to the client.
echo json_encode($output_arrays);