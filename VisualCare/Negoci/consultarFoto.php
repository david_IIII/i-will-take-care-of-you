<?php
// iniciamos sesion y guardamos el id de la sesion en una variable que se mandara en el curl
session_start();
$current_id = session_id();
//variable que guarda la imagen descodificada para tratarla
$imagen = base64_decode($_POST['imagen']);
//convertimos a imagen
$im = imagecreatefromstring($imagen);
if ($im !== false) {
    $path = getcwd();
    $path = str_replace("Negoci", "", $path);
    $path .=  "Fotografias/" . $_SESSION['num_historial'] . '/' . $_SESSION['dataForm'] . "_". $_SESSION['countFoto'] . '.jpg';
    $path = str_replace(" ", "_", $path);
    $_SESSION['countFoto'] += 1;
    
    imagejpeg($im, $path); //guardar a disco
    imagedestroy($im); //liberar memoria
}
$post = "path=" . $path . "&session=" . $current_id;
//cerramos la sesion
session_write_close();
//iniciamos una nueva sesion en el documento que insertara los valores en la base de datos
$ch = curl_init();
// definimos la URL a la que hacemos la petición
curl_setopt($ch, CURLOPT_URL, "http://localhost/M12/i-will-take-care-of-you/VisualCare/Persistencia/controlFoto.php");
// definimos el número de campos o parámetros que enviamos mediante POST
curl_setopt($ch, CURLOPT_POST, 1);
// definimos cada uno de los parámetros
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
// tranforma la respuesta en un string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// recibimos la respuesta y la guardamos en una variable
$response = curl_exec($ch);
curl_close($ch); // cerramos la sesión cURL
print_r($response);