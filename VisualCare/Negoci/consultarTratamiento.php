<?php
// iniciamos sesion y guardamos el id de la sesion en una variable que se mandara en el curl
session_start();
$current_id = session_id();
if(! isset($_POST['descripcion'])){
    $post = "idInfo=" . $_POST['idInfo'] . "&session=" . $current_id;
} else {
    $post = "idInfo=" . $_POST['idInfo'] . "&descripcion=" . $_POST['descripcion'] . "&session=" . $current_id;
}
//cerramos la sesion
session_write_close();
//iniciamos una nueva sesion en el documento que insertara los valores en la base de datos
$ch = curl_init();
// definimos la URL a la que hacemos la petición
curl_setopt($ch, CURLOPT_URL, "http://localhost/M12/i-will-take-care-of-you/VisualCare/Persistencia/controlTratamiento.php");
// definimos el número de campos o parámetros que enviamos mediante POST
curl_setopt($ch, CURLOPT_POST, 1);
// definimos cada uno de los parámetros
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
// tranforma la respuesta en un string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// recibimos la respuesta y la guardamos en una variable
$response = curl_exec($ch);
curl_close($ch); // cerramos la sesión cURL

if (isset($_SESSION['constantes'])) {
    $cambiar = array("[", "]");
    $confirmar = $_SESSION['constantes'];
    $consultar = str_replace($cambiar, "", $confirmar);
    $confirmar = explode(',', $confirmar);
    $primer = explode(";", $confirmar[0]);

    $html = '<table class="tablaConstantes" summary="Tabla que muestra las constantes del paciente" dir="ltr"><tbody><tr>';
    for ($i = 0; $i < (count($primer)); $i++) {
        $aux = explode('=', $primer[$i]);
        $html .= '<th>' . $aux[0]. '</th><td id="' . $aux[0] . '">' . $aux[1] . '</td></tr>';
    }
    $html .= '</tbody></table>';
    echo $html."','".$response;
    if($response === '1'){
        header('location:../Presentacio/inicioPaciente.php');
    }
}

