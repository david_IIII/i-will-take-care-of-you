<?php
// iniciamos sesion y guardamos el id de la sesion en una variable que se mandara en el curl
session_start();
$current_id = session_id();
// al clicar en boton enviar o foto del formulario_enfermera_control
if (isset($_GET['enviar']) || isset($_GET['foto'])) {
    //guardamos los valores del formulario en variables
    $tensionMax = floatval($_POST['tensionMaxima']);
    $tensionMin = floatval($_POST['tensionMinima']);
    $frecCardio = intval($_POST['frecCardio']);
    $temperatura = intval($_POST['temperatura']);
    $diuresis = (isset($_POST['diuresis']))?intval($_POST['diuresis']):0;
    $azucar = (isset($_POST['azucar']))?intval($_POST['azucar']):0;
    $descripcion = (isset($_POST['descripcion']))?$_POST['descripcion']:"sin comentarios";
    //cerramos la sesion
    session_write_close();
    //iniciamos una nueva sesion en el documento que insertara los valores en la base de datos
    $ch = curl_init();
    // definimos la URL a la que hacemos la petición
    curl_setopt($ch, CURLOPT_URL, "http://localhost/M12/i-will-take-care-of-you/VisualCare/Persistencia/controlFormulari.php");
    // definimos el número de campos o parámetros que enviamos mediante POST
    curl_setopt($ch, CURLOPT_POST, 1);
    // definimos cada uno de los parámetros
    curl_setopt($ch, CURLOPT_POSTFIELDS, "tensionMax=" . $tensionMax . "&tensionMin=" . $tensionMin . "&frecCardio=" . $frecCardio . "&temperatura=" . $temperatura . "&diuresis=" . $diuresis . "&azucar=" . $azucar . "&descripcion=" . $descripcion. "&session=".$current_id);
    // tranforma la respuesta en un string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // recibimos la respuesta y la guardamos en una variable
    $response = curl_exec($ch);
    curl_close($ch); // cerramos la sesión cURL
    if ($response && isset($_GET['enviar'])) {
        header("location:../Presentacio/inicioPaciente.php"); //devolvemos a l'usuario a la pagina de inicio del paciente 
    } else if($response && isset($_GET['foto'])) {
        $_SESSION['countFoto'] = 1;
        header('location:../Presentacio/foto.php'); //devolvemos a l'usuario a la pagina de inicio del paciente 
    }
}
