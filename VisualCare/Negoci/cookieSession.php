<?php

session_start();

if (isset($_POST['cookie'])) {
    $camas = json_decode($_POST['camas']);
    setcookie('cama', serialize($camas), time() + 60 * 60 * 24 * 7, '/');
    echo 'bien';
} else if (isset($_POST['session'])) {
    $_SESSION['cip'] = $_POST['cip'];
    $_SESSION['num_historial'] = $_POST['num_historial'];
    $_SESSION['nombre'] = $_POST['nombre'];
    $_SESSION['apellidos'] = $_POST['apellidos'];
    $_SESSION['fecha_nacimiento'] = $_POST['fecha_nacimiento'];
    echo 'bien';
} else if (isset($_POST['sesionPaciente'])) {
    $_SESSION['cip'] = null;
    $_SESSION['num_historial'] = null;
    $_SESSION['nombre'] = null;
    $_SESSION['apellidos'] = null;
    $_SESSION['fecha_nacimiento'] = null;
    $_SESSION['dataForm'] = null;
    echo 'bien';
}