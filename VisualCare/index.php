<?php
session_start();

// si el usuario ya ha accedido a la aplicacion anteriormente se le redirige a la pagina principal
if( isset($_SESSION['usuari']))
    header('location: Presentacio/principal.php');
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Iniciar Sessió</title>
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <!-- script para el modal recordar contrasena -->
        <script>
            $(document).ready(function () {
                $(document).keyup(function (e) {
                    if (e.keyCode == 27 && $('#myModal').css('display') === "block") {
                        $(".recordar").trigger('click');
                    }
                });
                $('.recordar').click(function () {
                    $('#email').val("");
                    $('#enviar').attr('disabled', 'disabled');
                });
                $('#email').keyup(function(){
                    if($('#email').val() !== "" ){
                        $('#enviar').removeAttr('disabled');
                    } else{
                        $('#enviar').attr('disabled', 'disabled');
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="global">
            <section class="globalOuter">
                <div class="cuadroDialogo">
                    <header id="header" class="cabecera">
                        <img src="img/logo_vallhebron1.png" width="100px" height="55px" alt="Logo del Hospital de la Vall d'Hebron de Barcelona" >
                    </header>
                    <!-- /header -->
                    <!-- formulario de acceso a la aplicación-->
                    <div class="principal">
                        <div class="login">
                            <h3 class="error"><?php print_r($_SESSION['error']); ?></h3>
                            <div class="formContainer" >
                                <form class="form-horizontal" role="form" action="Negoci/consultarLogin.php" method="post">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="user"  class="sr-only">Usuario</label>
                                            <input type="text" id="user" name="user" class="form-control" placeholder="Usuario" title="Nombre usuario" tabindex="1" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="passwd" class="sr-only">Contraseña</label>
                                            <input type="password" id="passwd" name="passwd" class="form-control" placeholder="Contraseña" title="Contraseña usuario" tabindex="2" />
                                            <a class="questionpass glyphicon glyphicon-question-sign recordar" href="#myModal" role="button" data-toggle="modal" data-target="#myModal" title="Recordar contraseña"></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-sm-offset-4 col-xs-offset-4 col-lg-offset-4">
                                            <input type="submit" class="btn btn-primary" id="entrar" name="entrar" role="Entrar" value="Entrar" tabindex="3">
                                        </div>
                                    </div>
                                    <!-- Modal Recordar contraseña -->
                                    <div id="myModal" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Recordar contraseña</h3>
                                                    <a class="cerrar glyphicon glyphicon-remove recordar" href="#myModal" role="button" data-toggle="modal" data-target="#myModal" title="Cerrar" accesskey="c"></a>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="email" class="control-label col-sm-2">Email:</label>
                                                        <div class="col-sm-9">
                                                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" title="Escribe tu dirección de correo"/>
                                                        </div>
                                                    </div>
                                                    <div class="btn-group col-sm-offset-2">
                                                        <input type="submit" class="btn btn-primary" id="enviar" name="enviar" value="Enviar" role="Enviar">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Fin Modal Recordar contraseña -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
</html>
