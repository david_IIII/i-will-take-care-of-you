<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient ('http://localhost:8080/WebService/WebServices?wsdl',array('cache_wsdl'=> WSDL_CACHE_NONE));
    if(isset($_POST['trata'])){
        //guardamos los valores del post en variables y castemos los valores para poder introducirlos directamente en la base de datos
        $addRequest = new stdClass();
        $addRequest->num_historial = intval($_SESSION['num_historial']);
        $respuesta = $client->mostrarTratamiento($addRequest);
    } else{
        $respuesta = $client->consultaGrafics();
    }
    print_r($respuesta->return);
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}
