<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
$_SESSION['mensaje'] = NULL;
$_SESSION['consultar'] = NULL;
$_SESSION['confirmar'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient ('http://localhost:8080/WebService/WebServices?wsdl',array('cache_wsdl'=> WSDL_CACHE_NONE));
    //guardamos los valores del post en variables y para poder consultar la base de datos
    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    //si los campos del formulario del paciente no estan vacios
    if (!empty($nombre) && !empty($apellidos)) {
        //comprobamos que los datos esten bien introducidos y los modificamos para que se pueda hacer la consulta
        /*$nombreCompuesto = explode(" ", $nombre);
        $primer = true;
        for ($i = 0; $i < count($nombreCompuesto); $i++) {
            $aux = trim($nombreCompuesto[$i]); //eliminamos los espacios en blanco(y otros caracteres) del final del string
            $aux = strtolower($aux); //convertimos el nombre a minusculas
            if ($primer) {
                //en el caso del primer nombre
                $nombre = $aux;
                $primer = false;
            } else {
                $nombre .= " " . $aux; //en el caso que tenga un nombre compuesto
            }
        }
        $primer = true;
        $apellidoCompuesto = explode(" ", $apellidos);
        for ($i = 0; $i < count($apellidoCompuesto); $i++) {
            $aux = trim($apellidoCompuesto[$i]); //eliminamos los espacios en blanco(y otros caracteres) del final del string
            $aux = strtolower($aux); //convertimos los apellidos a minusculas
            if ($primer) {
                $apellidos = $aux;
                $primer = false;
            } else {
                $apellidos .= " " . $aux;
            }
        }
        if(strcmp(substr($apellidos,-1)," "))
            $apellidos = substr($apellidos,0 , strlen($apellidos)-1);
        $apellidos = trim($apellidos);
        
        
        if(strcmp(substr($nombre,-1)," "))
            $nombre = substr($nombre,0 , strlen($nombre)-1);
        $nombre = trim($nombre);
        
        */
        
        $addRequest = new stdClass();
        $addRequest->nombre = $nombre;
        $addRequest->apellidos = $apellidos;
        //variable que guarda los datos devueltos en la consulta paciente
        $res = $client->consultaPacient($addRequest);
        //creamos una variable para guardar los datos de la consulta de constantes
        $resConstantes =  $client->consultaConstantes();
        if ($res->return){
            //guardamos los datos en una sesion
            $_SESSION['nombre'] = $nombre;
            $_SESSION['apellidos'] = $apellidos;
            $_SESSION['consultar'] = $res->return;
            echo $res->return;
        } else {
            $_SESSION['error'] = "El paciente no existe";
        }
        if($resConstantes->return){
            //guardamos el resultado en una sesion que despues recuperamos y tratamos en inicioPaciente para mostrar la informacion
            $_SESSION['constantes'] = $resConstantes->return;
        }
    } else {
        $_SESSION['error'] = "Alguno de los campos estan vacíos";
    }    
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}
