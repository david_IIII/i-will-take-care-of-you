<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient ('http://localhost:8080/WebService/WebServices?WSDL',array('cache_wsdl'=> WSDL_CACHE_NONE));
    //guardamos los valores del post en variables y castemos los valores para poder introducirlos directamente en la base
    $tensionMax = floatval($_POST['tensionMax']);
    $tensionMin = floatval($_POST['tensionMin']);
    $frecCardio = intval($_POST['frecCardio']);
    $temperatura = intval($_POST['temperatura']);
    $diuresis = (isset($_POST['diuresis']))?intval($_POST['diuresis']):0;
    $azucar = (isset($_POST['azucar']))?intval($_POST['azucar']):0;
    $descripcion = (isset($_POST['descripcion']))?$_POST['descripcion']:"sin comentarios";
    $addRequest = new stdClass();
    if (!empty($tensionMax) && !empty($tensionMin) && !empty($frecCardio) && !empty($temperatura)) {
        $addRequest->tensionMaxima = $tensionMax;
        $addRequest->tensionMinima = $tensionMin;
        $addRequest->frequencia = $frecCardio;
        $addRequest->temperatura = $temperatura;
        $addRequest->diuresis = $diuresis;
        $addRequest->azucar = $azucar;
        $addRequest->descripcion = $descripcion;
        $addRequest->num_historial = intval($_SESSION['num_historial']);
        $respuesta = $client->insertarFormulari($addRequest);
        if ($respuesta->return) {
            $_SESSION['dataForm'] = $respuesta->return;
            echo 'Datos del formulario introducidos correctamente';
        } else {
            //en el caso que el usuario no haya entrado los parametros en formato correcto
            echo $respuesta->return;
            $_SESSION['error'] = "Los datos introduidos no son correctos";
        }
    } else {
        //en el caso que los campos obligatoros esten vacios
        $_SESSION['error'] = "Los campos de tensio_max, tensio_min, frequencia_cardiaca i temperatura no pueden estar vacios";
    }
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}