<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient ('http://localhost:8080/WebService/WebServices?WSDL',array('cache_wsdl'=> WSDL_CACHE_NONE));
    //guardamos los valores del post en variables y castemos los valores para poder introducirlos directamente en la base de datos
    $addRequest = new stdClass();
    $addRequest->num_historial = intval($_SESSION['num_historial']);
    $addRequest->id = intval($_POST['idInfo']);
    if( ! isset($_POST['descripcion'])){
        $respuesta = $client->consultarTratamiento($addRequest);
    } else {
        $addRequest->descripcion = $_POST['descripcion'];
        $respuesta = $client->insertarTratamientos($addRequest);
    }
    if ($respuesta->return) {
        echo $respuesta->return;
    } else {
        //en el caso de que el usuario no haya introducido los parametros en formato correcto
        echo $respuesta->return;
        $_SESSION['error'] = "Los datos introducidos no son correctos";
    }
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}