<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient('http://localhost:8080/WebService/WebServices?wsdl',array('cache_wsdl'=> WSDL_CACHE_NONE));
    $user = $_POST['user'];
    $passwd = $_POST['passw'];
    if (!empty($user) && !empty($passwd)) {
        $addRequest = new stdClass();
        $addRequest->user = $user;
        $addRequest->passwd = $passwd;
        //llamamos a la funcion validaUsuari del webservice y entramos por parametros los valores del post
        $response = $client->validaUsuari($addRequest);
        if ($response->return) {
            //guarda la profesion del usuario(Medico, enfermero)
            $_SESSION['userProfesion'] = $response->return;
            $_SESSION['usuari'] = $user;
            $_SESSION['password'] = $passwd;
            echo 'Usuario introducido correcto!';
        } else {
            $_SESSION['error'] = "El usuario i/o contraseña introducido no es correcto!";
        }
    } else {
        $_SESSION['error'] = "Los campos de Usuario y Contraseña no pueden estar vacios!";
    }
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}
