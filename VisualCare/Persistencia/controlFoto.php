<?php
// no deja que se quede el wsdl en cache
ini_set('soap.wsdl_cache_enable',0);
// inicia la sesion con el id mandado en el curl
session_id($_POST['session']);
session_start();

$_SESSION['error'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient('http://localhost:8080/WebService/WebServices?WSDL', array('cache_wsdl' => WSDL_CACHE_NONE));
    //guardamos los valores del post en variables y castemos los valores para poder introducirlos directamente en la base
    $addRequest = new stdClass();
    $addRequest->data = $_SESSION['dataForm'];
    $addRequest->num_historial = intval($_SESSION['num_historial']);
    $addRequest->path = $_POST['path'];
    $respuesta = $client->insertarFoto($addRequest);
    if ($respuesta->return) {
        echo 'Fotografia introducida correctamente';
    } else {
        echo "error";
        $_SESSION['error'] = "La imagen no se ha creado ";
    }
} catch (SoapFault $e) {
    var_dump($e);
    $_SESSION['error'] = "Error en la conexión.";
}
