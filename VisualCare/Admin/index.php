<?php
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>WebService2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- JQUERY -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/pagPrincipalAdmin.css">
    </head>
    <body>
        <div class="global">
            <section class="globalOuter">
                <div class="cuadroDialogo">
                    <legend id="header" class="cabecera">
                        <h1 class="logo">Administrador</h1>
                    </legend>
                    <div class="principal">
                        <div class="login">
                            <h3><?php
                                $err = $_SESSION['error'];
                                $men = $_SESSION['mensaje'];
                                if ($err != null) {
                                    echo "<b><span class='error'>" . $err . "</span></b>";
                                } else if ($men != null) {
                                    echo "<b><span class='pass'>" . $men . "</span></b>";
                                }
                                ?>
                            </h3>
                            <div class="formContainer" >
                                <form class="form-horizontal" role="form" action="Persistencia/controlUsuari.php" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-12 col-xs-12 col-md-12">
                                            <label class="sr-only" for="user">Usuario</label>

                                            <input type="text" class="form-control" id="user" name="user" placeholder="Usuario" title="Nombre usuario" tabindex="1"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="sr-only" for="passw">Contraseña</label>
                                            <input type="text" class="form-control" id="passw" name="passw" placeholder="Contraseña" title="Contraseña usuario" tabindex="2"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="sr-only" for="id">Id Personal</label>
                                            <input type="text" class="form-control" id="id" name="id" placeholder="Personal Id" title="Codigo de identificacion del usuario" tabindex="3"/>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <input class="btn btn-primary" type="submit" value="Alta" name="alta" tabindex="4"/>
                                        <input class="btn btn-primary" type="submit" value="Baixa" name="baixa" tabindex="5"/>
                                        <input class="btn btn-primary" type="submit" value="Modificar" name="modifica" tabindex="6"/>
                                        <input class="btn btn-primary" type="submit" value="Consultar" name="consulta" tabindex="7"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
            if (isset($_SESSION['consulta'])) {
                $cambiar = array("[", "]");
                $consulta = $_SESSION['consulta'];
                $consulta = str_replace($cambiar, "", $consulta);
                $consulta = explode(',', $consulta);
                $primer = explode(";", $consulta[0]);
                echo 
                '<table class="tabla" summary="Tabla que muestra los usuarios registrados y sus datos" dir="ltr">
                    <thead>
                        <tr>';
                for ($i = 0; $i < count($primer); $i++) {
                    $aux = explode(':', $primer[$i]);
                    echo '<th>'.$aux[0].'</th>';
                }
                echo  
                        '</tr>
                    </thead>
                    <tbody>';
                for ($i = 0; $i < count($consulta); $i++) {
                    echo '<tr>';
                    $primer = explode(";", $consulta[$i]);
                    for($j = 0; $j < count($primer); $j++ ){
                        $aux = explode(':', $primer[$j]);
                        echo '<td>'.$aux[1].'</td>';
                    }
                    echo '</tr>';
                }
                echo '</tbody>
                </table>';
            }
            ?>
        </div>
    </body>
</html>
