<?php

session_start();
$_SESSION['error'] = NULL;
$_SESSION['mensaje'] = NULL;
$_SESSION['consulta'] = NULL;
try {
    // en la variable $client guardamos la conexion con el servidor de aplicaciones
    $client = new SoapClient('http://localhost:8080/WebService/WebServiceAdmin?WSDL');
    //guardamos los valores del formulario en variables
    $user = $_POST['user'];
    $passwd = $_POST['passw'];
    $id = $_POST['id'];
    if (!empty($user) && !empty($passwd)) {
        $addRequest = new stdClass();
        $addRequest->usuari = $user;
        $addRequest->contrasenya = $passwd;
        if (isset($_POST['alta'])) {
            if (!empty($id)) {
                $addRequest->identificador = intval($id);
                $response = $client->altaUsuari($addRequest);
                if ($response->return) {
                    $_SESSION['mensaje'] = "Usuario dado de alta correctamente!";
                } else {
                    $_SESSION['error'] = "El usuario no se ha dado de alta!";
                }
            } else {
                $_SESSION['error'] = "El campo de Id Personal no puede estar vacio!";
            }
        } else if (isset($_POST['baixa'])) {
            $response = $client->baixaUsuari($addRequest);
            if ($response->return) {
                $_SESSION['mensaje'] = "El usuario se ha dado de baja correctamente!";
            } else {
                $_SESSION['error'] = "El usuario no se ha podido dar de baja!";
            }
        } else if (isset($_POST['modifica'])) {
            $response = $client->modificaUsuari($addRequest);
            if ($response->return) {
                $_SESSION['mensaje'] = "El usuario se ha modificado correctamente!";
            } else {
                $_SESSION['error'] = "El usuario no se ha modificado!";
            }
        }
    } else {
        if (isset($_POST['consulta'])) {
            $response = $client->consultaContrasenyes();
            $_SESSION['consulta'] = $response->return;
        } else if (isset($_POST['alta'])) {
            $_SESSION['error'] = "Los campos de Usuario, Contraseña y Id Personal no pueden estar vacios!";
        } else {
            $_SESSION['error'] = "Los campos de Usuario y Contraseña no pueden estar vacios!";
        }
    }
} catch (SoapFault $e) {
    $_SESSION['error'] = $e;
    var_dump($e);
}

header('location:../index.php');
