// funcion para cerrar el formulario y que se vuelva ha mostrar el contenido inicial
function cerrarForm(data){
    $(data).hide();
    $(".cuerpo").show();
}
// funcion para mostrar el formulario de control de los enfermeros
function mostrar() {
    $(".cuerpo").hide();
    document.getElementById("mostrarInfo").innerHTML = '<form  id="formularioEnfermera" class="form-horizontal" name="formularioEnfermera" role="form" method="post">'
            +'<div class="titolForm">'
                +'<h1 class="text-center col-xs-11">Formulario Control Paciente</h1>'
                +'<button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#formularioEnfermera\')" onkeypress="cerrarForm(\'#formularioEnfermera\')" title="Cerrar formulario"></button>'
            +'</div>' 
            +'<div class="form-group">'
                +'<label for="tensionMaxima" class="control-label col-sm-2">Tension arterial máxima:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="6" max="25" step="0.1" id="tensionMaxima" name="tensionMaxima" title="Escribe la tension arterial máxima">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="tensionMinima" class="control-label col-sm-2">Tension arterial mínima:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="2" max="15" step="0.1" id="tensionMinima" name="tensionMinima" title="Escribe la tension arterial mínima">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="frecCardio" class="control-label col-sm-2">Frecuencia Cardiaca:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="40" max="210" id="frecCardio" name="frecCardio" title="Escribe la frecuencia cardíaca">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="temperatura" class="control-label col-sm-2">Temperatura:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="30" max="43" step="0.1" id="temperatura" name="temperatura" title="Escribe la temperatura">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="diuresis" class="control-label col-sm-2">Diuresis:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="0" max="5000" id="diuresis" name="diuresis" title="Escribe la diuresis">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="azucar" class="control-label col-sm-2">Azucar:</label>'
                +'<div class="col-sm-9">'
                    +'<input type="number" class="form-control" min="30" max="450" id="azucar" name="azucar" title="Escribe el azucar">'
                +'</div>'
            +'</div>'
            +'<div class="form-group">'
                +'<label for="descripcion" class="control-label col-sm-2">Descripción:</label>'
                +'<div class="col-sm-9">'
                    +'<textarea rows="20" cols="40" name="descripcion" id="descripcion" class="form-control" form="formularioEnfermera" title="Escribe una descripción">Añade descripcion aqui</textarea>'
                +'</div>'
            +'</div>'
            +'<div class="btn-group col-sm-offset-2">'
                +'<input type="button" class="btn-primary btn-lg" id="enviar" name="enviar" value="Enviar" role="Enviar">&nbsp'
                +'<button type="button" class="btn-primary btn-lg" id="foto" name="foto" value="Foto" role="Foto" title="Hacer foto"><span class="glyphicon glyphicon-camera" aria-hidden="true"></span></button>'
            +'</div>'
        +'</form>';
    //evento que activa la funcion del control de errores y envia los valores del formulario
    $('#enviar').click(function(){
        var error = false;
        if (! $('#tensionMaxima').val() || ! $.isNumeric($('#tensionMaxima').val()) || parseInt($('#tensionMaxima').val()) > 25 || parseInt($('#tensionMaxima').val()) < 6){
            $('#tensionMaxima').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#tensionMaxima').parent().parent().removeClass('has-error');
        }
        if (! $('#tensionMinima').val() || ! $.isNumeric($('#tensionMinima').val()) || parseInt($('#tensionMinima').val()) > 15 || parseInt($('#tensionMinima').val()) < 2){
            $('#tensionMinima').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#tensionMinima').parent().parent().removeClass('has-error');
        }
        if (! $('#frecCardio').val() || ! $.isNumeric($('#frecCardio').val())){
            $('#frecCardio').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#frecCardio').parent().parent().removeClass('has-error');
        }
        if (! $('#temperatura').val() || ! $.isNumeric($('#temperatura').val()) || parseInt($('#temperatura').val()) > 43 || parseInt($('#temperatura').val()) < 30){
            $('#temperatura').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#temperatura').parent().parent().removeClass('has-error');
        }
        if (! $('#diuresis').val() || ! $.isNumeric($('#diuresis').val())){
            $('#diuresis').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#diuresis').parent().parent().removeClass('has-error');
        }
        if (! $('#azucar').val() || ! $.isNumeric($('#azucar').val())){
            $('#azucar').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#azucar').parent().parent().removeClass('has-error');
        }
        if(!error){
            $("#formularioEnfermera").attr("action", "../Negoci/consultarFormulario.php?enviar=Enviar");
            $('#formularioEnfermera').submit();
        }
    });
    //evento que activa la funcion del control de errores, envia los valores del formulario y nos redirige a la pagina para capturar las imagenes
    $('#foto').click(function(){
        var error = false;
        if (! $('#tensionMaxima').val() || ! $.isNumeric($('#tensionMaxima').val()) || parseInt($('#tensionMaxima').val()) > 25 || parseInt($('#tensionMaxima').val()) < 6 ){
            $('#tensionMaxima').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#tensionMaxima').parent().parent().removeClass('has-error');
        }
        if (! $('#tensionMinima').val() || ! $.isNumeric($('#tensionMinima').val()) || parseInt($('#tensionMinima').val()) > 15 || parseInt($('#tensionMinima').val()) < 2){
            $('#tensionMinima').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#tensionMinima').parent().parent().removeClass('has-error');
        }
        if (! $('#frecCardio').val() || ! $.isNumeric($('#frecCardio').val())){
            $('#frecCardio').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#frecCardio').parent().parent().removeClass('has-error');
        }
        if (! $('#temperatura').val() || ! $.isNumeric($('#temperatura').val()) || parseInt($('#temperatura').val()) > 43 || parseInt($('#temperatura').val()) < 30){
            $('#temperatura').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#temperatura').parent().parent().removeClass('has-error');
        }
        if (! $('#diuresis').val() || ! $.isNumeric($('#diuresis').val())){
            $('#diuresis').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#diuresis').parent().parent().removeClass('has-error');
        }
        if (! $('#azucar').val() || ! $.isNumeric($('#azucar').val())){
            $('#azucar').parent().parent().addClass('has-error');
            error= true;
        } else{
            $('#azucar').parent().parent().removeClass('has-error');
        }
        if(!error){
            $("#formularioEnfermera").attr("action", "../Negoci/consultarFormulario.php?foto=Foto");
            $('#formularioEnfermera').submit();
        }
    });
}

//funcion que muestra las constantes del paciente y un formulario para que el medico pueda introducir el tratamiento a seguir
function mostrarTratamiento() {
    $(".cuerpo").hide();
    // mostramos la lista de los tratamientos abiertos
    $.ajax({
        url: '../Negoci/consultarInforme.php',
        async: true,
        success: function (szData) {
            var data = szData.replace('[','');
            data = data.replace(']','');
            data = data.split(',');
            var html = '<div class="container" id="informeList"><div><button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false" title="Cerrar formulario" onclick="cerrarForm(\'#informeList\')" onkeypress="cerrarForm(\'#informeList\')"></button></div>';
            for (var i = 0; i < data.length; i++) {
                html += '<div class="row">';
                var primer = data[i].split(';');
                for(var j = 0; j < primer.length; j++ ){
                    var aux = primer[j].split('=');
                    if(aux[0] === 'Id'){
                        html += '<input type="hidden" id="id'+aux[1]+'" name="id'+aux[1]+'" value="'+aux[1]+'">';
                    }
                    if(aux[0] === 'Patología') {
                        html += '<div class="text-center table-border"><button class="btn-primary listInforme" type="button" aria-expanded="false">' + aux[1] + '</button></div>';
                    }
                }
                html += '</div>';
            }
            $('#mostrarInfo').html(html+"</div>");
            //mostramos las ultimas constantes del paciente
            $('.listInforme').click(function(){
                var idInfo = $(this).parent().parent().find('input').attr('id');
                var num = idInfo.charAt((idInfo.length - 1));
                $.ajax({
                    url: '../Negoci/consultarTratamiento.php',
                    type: 'post',
                    async: true,data: {
                        'idInfo': num
                    }, success: function (data) {
                        var datas = data.split('\',\'');
                        //mostramos el formulario del medico
                        $("#mostrarInfo").html('<form  id="formularioMedico" class="form-horizontal" name="formularioMedico" role="form" method="post">'
                        +'<div class="container">'
                        +'<div class="titolForm">'
                            +'<h1 class="text-center col-xs-11">Tratamiento paciente</h1>'
                            +'<button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#formularioMedico\')"></button>'
                        +'</div>'
                        +'<div class="tablaUltimForm col-xs-offset-3">'+ datas[0] + '</div>'
                        +'<div class="form-group">'
                            +'<label for="descripcion" class="control-label col-sm-2">Descripción:</label>'
                            +'<div class="col-sm-9">'
                                +'<textarea rows="20" cols="40" name="descripcion" id="descripcion" class="form-control" form="formularioMedico" title="Escribe el tratamiento">'+ datas[1] +'</textarea>'
                            +'</div>'
                        +'</div>'
                            +'<div>'
                            +'<input type="button" class="btn-primary btn-lg" id="modificar" name="modificar" value="Modificar" role="Modificar">'
                            +'<input type="button" class="btn-primary btn-lg pull-right" id="mantener" name="mantener" value="Mantener Tratamiento" role="Mantener Tratamiento">'
                        +'</div>'
                        +'</div>'
                    +'</form>');
                        //evento para enviar las modificaciones a la base de datos
                        $('#modificar').click(function(){
                            $("#formularioMedico").attr("action", "../Negoci/consultarTratamiento.php");
                            $("#formularioMedico").append("<input type='hidden' name='idInfo' value='"+num+"'>")
                            $('#formularioMedico').submit();
                        });
                        //evento para volver a la pagina del paciente
                        $('#mantener').click(function(){
                            $('#formularioMedico').hide();
                            $('.cuerpo').show();
                        });
                    }
                });
            });
        },
        error: function () {
            alert('error');
        }
    });
}
// funcion para mostrar la lista de informes de un paciente
function informe(){
    $(".cuerpo").hide();
    //muestra la lista de informes
    $.ajax({
        url: '../Negoci/consultarInforme.php',
        async: true,
        success: function (szData) {
            var data = szData.replace('[','');
            data = data.replace(']','');
            data = data.split(',');
            var html = '<div class="container" id="informeList"><div><button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#informeList\')" onkeypress="cerrarForm(\'#informeList\')" title="Cerrar informe"></button></div>';
            for (var i = 0; i < data.length; i++) {
                html += '<div class="row">';
                var primer = data[i].split(';');
                for(var j = 0; j < primer.length; j++ ){
                    var aux = primer[j].split('=');
                    if(aux[0] === 'Id'){
                        html += '<input type="hidden" id="id'+aux[1]+'" name="id'+aux[1]+'" value="'+aux[1]+'">';
                    }
                    if(aux[0] === 'Patología') {
                        html += '<div class="text-center table-border"><button class="btn-primary listInforme" type="button" aria-expanded="false">' + aux[1] + '</button></div>';
                    }
                }
                html += '</div>';
            }
            $('#mostrarInfo').html(html+"</div>");
            $('.listInforme').click(function(){
                var idInfo = $(this).parent().parent().find('input').attr('id');
                var num = idInfo.charAt((idInfo.length - 1));
                // muestra los formularios y pruebas del informe seleccionado
                $.ajax({
                    url: '../Negoci/consultarInforme.php',
                    type: 'post',
                    async: true,
                    data: {
                        'idInfo': num
                    }, success: function (szData) {
                        var datos = szData.split('],,[');
                        var html="";
                        for (var k = 0; k < datos.length; k++){
                            var Controles;
                            if (k == 0){
                                Controles = "Controles";
                            }else {
                                Controles = "Pruebas";
                            }
                            var data = datos[k].replace('[','');
                            data = data.split('],');
                            html += '<div class="container" id="formList"><div><h1 class="text-center">'+Controles+'</h1><div><button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#formList\')" onkeypress="cerrarForm(\'#formList\')" title="Cerrar informe"></button></div>';
                            if(Controles === "Controles")
                                html += '<div class="row text-center"><div class="col-xs-2"><b>Fecha</b></div><div class="col-xs-1"><b>Número Historial</b></div><div class="col-xs-1"><b>Tensión Maxima</b></div><div class="col-xs-1"><b>Tensión Minima</b></div><div class="col-xs-1"><b>Frecuencia Cardíaca</b></div><div class="col-xs-1"><b>Temperatura</b></div><div class="col-xs-1"><b>Diuresis</b></div><div class="col-xs-1"><b>Glicemia</b></div><div class="col-xs-3"><b>Descripción</b></div></div>';
                            for (var i = 0; i < data.length; i++) {
                                html += '<hr><div class="row">';
                                data[i] = data[i].replace(']','');
                                data[i] = data[i].replace('[','');
                                var primer = data[i].split(';');
                                for(var j = 0; j < primer.length; j++ ){
                                    var aux = primer[j].split('=');
                                    //mostrar las fotos de los formularios
                                    if(aux[0].indexOf("Foto") > -1){
                                        var fotos = aux[1].split(',');
                                        if(fotos.length >  0){
                                            html += '</div><div class="row">';
                                        }
                                        for(var m = 0; m < fotos.length;m++){
                                            var foto = fotos[m].split('::');
                                            if (foto[0] === 'Path' ){
                                                var path = "";
                                                var pathPart = foto[1].split('/');
                                                for(var l in pathPart ){
                                                    if(pathPart[l] === '' || pathPart[l] === 'var'|| pathPart[l] === 'www' || pathPart[l] === 'html') {  
                                                    } else {
                                                        path += "/" + pathPart[l];
                                                    }
                                                }
                                                html += '<div class="col-xs-2"><img src="' + path + '" class="img-responsive"></div>';
                                            }
                                        }
                                        if(fotos.length >  0){
                                            html += '</div>';
                                        }
                                    } else if(typeof aux[1] !== 'undefined' && aux[0] === 'Data') {
                                        html += '<div class="col-xs-2">' + aux[1] + '</div>';
                                    } else if(typeof aux[1] !== 'undefined' && aux[0] === 'Descripción') {
                                        html += '<div class="col-xs-3 text-justify">' + aux[1] + '</div>';
                                    } else if(typeof aux[1] !== 'undefined') {
                                        html += '<div class="col-xs-1">' + aux[1] + '</div>';
                                    }
                                }
                                if(fotos.length ==  0){
                                    html += '</div>';
                                }
                            }
                            html += "</div>";
                        }
                        $('#mostrarInfo').html(html+"</div>");
                    }, error: function (data) {
                        alert('error');
                        //alert(JSON.stringify(data));
                    }
                });
            });
        },
        error: function () {
            alert('error');
        }
    });
}
// funcion para mostrar todos los informes tanto abiertos como cerrados
function historial(){
    $(".cuerpo").hide();
    //muestra la lista de informes
    $.ajax({
        url: '../Negoci/consultarHistorial.php',
        async: true,
        success: function (szData) {
            var data = szData.replace('[','');
            data = data.replace(']','');
            data = data.split(',');
            var html = '<div class="container" id="informeList"><div><button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#informeList\')" onkeypress="cerrarForm(\'#informeList\')" title="Cerrar historial"></button></div>';
            for (var i = 0; i < data.length; i++) {
                html += '<div class="row">';
                var primer = data[i].split(';');
                for(var j = 0; j < primer.length; j++ ){
                    var aux = primer[j].split('=');
                    if(aux[0] === 'Id'){
                        html += '<input type="hidden" id="id'+aux[1]+'" name="id'+aux[1]+'" value="'+aux[1]+'">';
                    }
                    if(aux[0] === 'Patología') {
                        html += '<div class="text-center table-border"><button class="btn-primary listInforme">' + aux[1] + '</button></div>';
                    }
                }
                html += '</div>';
            }
            $('#mostrarInfo').html(html+"</div>");
            $('.listInforme').click(function(){
                var idInfo = $(this).parent().parent().find('input').attr('id');
                var num = idInfo.charAt((idInfo.length - 1));
                // muestra los formularios y pruebas del informe seleccionado
                $.ajax({
                    url: '../Negoci/consultarInforme.php',
                    type: 'post',
                    async: true,
                    //dataType: 'json',
                    data: {
                        'idInfo': num
                    }, success: function (szData) {
                        var datos = szData.split('],,[');
                        //alert(datos);
                        var html="";
                        for (var k = 0; k < datos.length; k++){
                            var Controles;
                            if (k == 0){
                                Controles = "Controles";
                            }else {
                                Controles = "Pruebas";
                            }
                            var data = datos[k].replace('[','');
                            data = data.split('],');
                            html += '<div class="container" id="formList"><div><h1 class="text-center">'+Controles+'</h1><div><button id="cerrar" class="btn-primary glyphicon glyphicon-remove cerrarForm col-xs-offset-11" type="button"  aria-expanded="false"  onclick="cerrarForm(\'#formList\')" onkeypress="cerrarForm(\'#formList\')" title="Cerrar historial"></button></div>';
                            if(Controles === "Controles")
                                html += '<div class="row text-center"><div class="col-xs-2"><b>Fecha</b></div><div class="col-xs-1"><b>Número Historial</b></div><div class="col-xs-1"><b>Tensión Maxima</b></div><div class="col-xs-1"><b>Tensión Minima</b></div><div class="col-xs-1"><b>Frecuencia Cardíaca</b></div><div class="col-xs-1"><b>Temperatura</b></div><div class="col-xs-1"><b>Diuresis</b></div><div class="col-xs-1"><b>Glicemia</b></div><div class="col-xs-3"><b>Descripción</b></div></div>';
                            for (var i = 0; i < data.length; i++) {
                                html += '<hr><div class="row">';
                                data[i] = data[i].replace(']','');
                                data[i] = data[i].replace('[','');
                                var primer = data[i].split(';');
                                for(var j = 0; j < primer.length; j++ ){
                                    var aux = primer[j].split('=');
                                    //mostrar las fotos de los formularios
                                    if(aux[0].indexOf("Foto") > -1){
                                        var fotos = aux[1].split(',');
                                        if(fotos.length >  0){
                                            html += '</div><div class="row">';
                                        }
                                        for(var m = 0; m < fotos.length;m++){
                                            var foto = fotos[m].split('::');
                                            if (foto[0] === 'Path' ){
                                                var path = "";
                                                var pathPart = foto[1].split('/');
                                                for(var l in pathPart ){
                                                    if(pathPart[l] === '' || pathPart[l] === 'var'|| pathPart[l] === 'www' || pathPart[l] === 'html') {  
                                                    } else {
                                                        path += "/" + pathPart[l];
                                                    }
                                                }
                                                html += '<div class="col-xs-2"><img src="' + path + '" class="img-responsive"></div>';
                                            }
                                        }
                                        if(fotos.length >  0){
                                            html += '</div>';
                                        }
                                    } else if(typeof aux[1] !== 'undefined' && aux[0] === 'Data') {
                                        html += '<div class="col-xs-2">' + aux[1] + '</div>';
                                    } else if(typeof aux[1] !== 'undefined' && aux[0] === 'Descripción') {
                                        html += '<div class="col-xs-3 text-justify">' + aux[1] + '</div>';
                                    } else if(typeof aux[1] !== 'undefined') {
                                        html += '<div class="col-xs-1">' + aux[1] + '</div>';
                                    }
                                }
                                if(fotos.length ==  0){
                                    html += '</div>';
                                }
                            }
                            html += "</div>";
                        }
                        $('#mostrarInfo').html(html+"</div>");
                    }, error: function (data) {
                        alert('error');
                        //alert(JSON.stringify(data));
                    }
                });
            });
        },
        error: function () {
            alert('error');
        }
    });
}
//funcion para borrar los datos de la sesion del paciente
function paciente(){
    $.ajax({
        url: '../Negoci/cookieSession.php',
        type: 'post',
        async: true,
        data: {
            'sesionPaciente': true 
        }, success: function () {
            //redirige al ususario a la pagina principal para consultar otro paciente
            setTimeout("location.href='principal.php'", 500);
        }, error: function () {
            alert('error');
        }
    });
}

$(document).ready(function(){
    var width = $('.tratamiento').width();
    $("#grafic").width(width);
    if(width < 600){
        $("#grafic").height(500);
    }
    var ctx = $("#grafic").get(0).getContext("2d");
    //consulta los datos del paciente para hacer el grafico
    $.ajax({
        url: '../Negoci/graficsTratamiento.php',
        async: false,
        success: function (szData) {
            var fechas = [];
            var tensionMax = [];
            var tensionMin = [];
            var frecCard = [];
            var temperatura = [];
            var datas = szData.replace('[','');
            datas = datas.replace(']','');
            datas = datas.split(',');
            for(var i = 0; i < datas.length; i++){
                var aux = datas[i].split(';');
                var fechaTemp = aux[0].split('=');
                var fecha = fechaTemp[1].split('.');
                var tensionMaxima = aux[2].split('=');
                var tensionMinima = aux[3].split('=');
                var frec = aux[4].split('=');
                var temp = aux[5].split('=');
                fechas[i] = fecha[0];
                tensionMax[i] = tensionMaxima[1];
                tensionMin[i] = tensionMinima[1];
                frecCard[i] = frec[1];
                temperatura[i] = temp[1];
            }
            var data = generateData(fechas,tensionMax,tensionMin,frecCard,temperatura);
            Chart.defaults.global.multiTooltipTemplate = "<%= datasetLabel %>: <%= value %>";
            Chart.defaults.global.tooltipTemplate = "<%if (datasetLabel){%><%=datasetLabel%>: <%}%><%if (label){%><%=label%>: <%}%><%= value %>";
            new Chart(ctx).Line(data);
        }
    });
    // muestra la lista de tratamientos que sigue el paciente en la actualidad
    $.ajax({
        url: '../Negoci/graficsTratamiento.php',
        type: 'post',
        async: false,
        data: {
          'trata': 1
        },
        success: function (szData) {
            var datas = szData.replace('[','');
            datas = datas.replace(']','');
            datas = datas.split(',');
            var html= "<div><h1 class='text-center'>Tratamiento Actual</h1>";
            for(var i = 0; i < datas.length; i++){
                var tratamiento = datas[i].split(';');
                var descripcion="";
                var fecha = "";
                var pato = "";
                for(var j = 0; j < tratamiento.length; j++){
                    var aux = tratamiento[j].split('=');
                    if(aux[0] === 'Fecha inicio'){
                        var fech = aux[1].split('.')
                        fecha = "Fecha inicio: "+fech[0];
                    } else if(aux[0] === 'Patología'){
                        pato = aux[1];
                    } else if(aux[0] === 'Descripción'){
                        descripcion = "Descripción: "+aux[1];
                    }
                }
                html += "<hr><div class='row'> <div class='col-sm-offset-1 col-sm-5 text-center'><b>"+pato+"</b></div><div class='col-sm-6 text-right'>"+fecha+"</div></div><div class='row'><div class='col-xs-offset-1'>"+descripcion+"</div></div>";
            }
            $('.tratamiento').html(html+"<br></div>");
        }
    });
});

// funcion para crear los datos del grafico
function generateData(fechas,tensMax,tensMin,frec,temp){
    var data = { 
        labels: fechas,
        datasets: [
            {
                label: "Tension Arterial Maxima",
                fillColor: "rgba(0,255,0,0.2)",
                strokeColor: "rgba(0,255,0,1)",
                pointColor: "rgba(0,255,0,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: tensMax
            },
            {
                label: "Tension Arterial Minima",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: tensMin
            },
            {
                label: "Frecuencia Cardiaca",
                fillColor: "rgba(187,72,72,0.2)",
                strokeColor: "rgba(187,72,72,1)",
                pointColor: "rgba(187,72,72,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: frec
            },
            {
                label: "Temperatura",
                fillColor: "rgba(197,183,115,0.2)",
                strokeColor: "rgba(197,183,115,1)",
                pointColor: "rgba(197,183,115,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: temp
            }
        ]
    };    
    return data;
}