$(document).ready(function () {
    // inicia el calendario con los datos del usuario registrado.
    $('.calendario').fullCalendar({
        header: {
            left: 'prev today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay next'
        },
        defaultDate: '2015-02-26',
        titleFormat: 'MMM YY',
        firstDay: 1,
        fixedWeekCount: false,
        timezone: 'local',
        editable: false,
        eventLimit: true,
        events: {
            url: '../Negoci/calendar.php',
            type: 'POST',
            data: {
                'user': usuari
            },
            error: function (data) {
                alert('error!');
                //alert(JSON.stringify(data));
            },
            color: 'black',
            textColor: 'yellow'
        },
        timeFormat: 'H:mm',
        dayClick: function (date) {
            var titulo = prompt("Escriba el titulo del evento", "Reunion Jefe personal");
            if (titulo) {
                var source = {
                    'title': titulo,
                    'start': date.format()
                };
                //se incia un ajax para poder reescribir el archivo json que contiene los eventos del calendario
                $.ajax({
                    url: '../Negoci/rewritejson.php',
                    type: 'POST',
                    async: true,
                    data: {
                        'title': titulo,
                        'start': date.format(),
                        'user': usuari,
                        'insertar': true
                    },
                    success: function (data) {
                        if (data) {
                            alert('evento creado');
                            $('.calendario').fullCalendar('renderEvent', source);
                        }
                    },
                    error: function () {
                        alert('error');
                    }
                });
            }
        },
        eventClick: function (event) {
            //antes de eliminar pregunta si quieres eliminarlo.
            if (confirm('quiere eliminar el evento?')) {
                //se incia un ajax para poder reescribir el archivo json que contiene los eventos del calendario
                $.ajax({
                    url: '../Negoci/rewritejson.php',
                    type: 'POST',
                    async: true,
                    data: {
                        'evento': JSON.stringify(event),
                        'user': usuari,
                        'eliminar': true
                    },
                    success: function () {
                        alert('evento eliminado');
                        $('.calendario').fullCalendar('removeEvents', event._id);
                    },
                    error: function () {
                        alert('error');
                    }
                });
            }
        }
    });
    // al hacer click en el boton que aparece en la tabla de los pacientes se inicia la funcion para guardar los datos del paciente en sesiones
    $('.confirmar').click(function () {
        var idBoton = $(this).attr("id");
        var num = idBoton.charAt((idBoton.length - 1));
        var cip = $('#CIP' + num).html();
        var num_historial = $('#num_historial' + num).html();
        var nombre = $('#Nombre' + num).html();
        var apellidos = $('#Apellidos' + num).html();
        var fecha_nacimiento = $('#Fecha_nacimiento' + num).html();
        //ajax para guardar los datos en sesiones
        $.ajax({
            url: '../Negoci/cookieSession.php',
            type: 'post',
            async: true,
            data: {
                'session': true,
                'cip': cip,
                'num_historial': num_historial,
                'nombre': nombre,
                'apellidos': apellidos,
                'fecha_nacimiento': fecha_nacimiento
            }, success: function (data) {
                //alert('be');
                //alert(JSON.stringify(data));
            }, error: function (data) {
                alert('error');
                //alert(JSON.stringify(data));
            }
        });

    });
    // evento que actualiza el estado de las camas
    $('.bed').click(function () {
        $(this).toggleClass('redBed');
        for (var i = 1; i <= 20; i++) {
            if ($(this).attr('id') == i) {
                cama[i - 1] = ($(this).hasClass('redBed')) ? 1 : 0;
            }
        }
        // ajax para guardar en cookie las camas modificadas
        $.ajax({
            url: '../Negoci/cookieSession.php',
            type: 'POST',
            async: true,
            data: {
                'cookie': true,
                'camas': JSON.stringify(cama)
            },
            error: function () {
                alert('error');
            }
        });
    });
});
