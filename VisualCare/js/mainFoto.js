var $canvas;
var ctx;
var $video;
$(document).ready(function () {
    $canvas = $('#screenshot-canvas');
    ctx = $canvas[0].getContext('2d');
    $video = $('#screenshot-stream')[0];

    if (navigator.getUserMedia) {
        navigator.getUserMedia({'video': true}, function (stream) {
            $video.src = stream;
            sizeCanvas();
            $video.play();
        }, errorCallback);
    } else if (navigator.webkitGetUserMedia) {
        navigator.webkitGetUserMedia({'video': true}, function (stream) {
            $video.src = window.URL.createObjectURL(stream);
            sizeCanvas();
            $video.play();
        }, errorCallback);
    } else if (navigator.mozGetUserMedia) {
        navigator.mozGetUserMedia({'video': true}, function (stream) {
            $video.mozSrcObject = stream;
            sizeCanvas();
            $video.play();
        }, errorCallback);
    } else {
        errorCallback({target: $video});
    }
    $('#screenshot-enviar-button').attr('disabled', 'disabled');
    $('#screenshot-button').click(function () {
        snapshot();
        if ($(this).html() === "Reintentar") {
            $(this).html("Capturar");
            $('#screenshot-enviar-button').attr('disabled', 'disabled');
            $video.play();
        } else {
            $(this).html("Reintentar");
            $('#screenshot-enviar-button').removeAttr('disabled');
            $video.pause();
        }
    });

    $video.click(function (e) {
        snapshot();
        $('#screenshot-button').html("Reintentar");
        $video.play();
    });

    $('#screenshot-enviar-button').click(function () {
        imagenSend();
    });
});

function sizeCanvas() {
    $canvas[0].width = $video.videoWidth;
    $canvas[0].height = $video.videoHeight;
    ctx = $canvas[0].getContext('2d');
}

function snapshot() {
    sizeCanvas();
    ctx.drawImage($video, 0, 0);
}

function errorCallback(e) {
    if (e.code === 1) {
        alert('El usuario no tiene acceso a la cámara');
    } else {
        alert('Error: ' + e);
    }
}

function imagenSend() {
    // se inicializa un form para mandar los datos
    var formdata = new FormData();
    // guarda los datos de la imagen en base 64
    var canva = $canvas[0].toDataURL('image/jpeg').split(',')[1];
    // lo añade form
    formdata.append("imagen", canva);
    // ajax para guardar la foto
    $.ajax({
        url: "../Negoci/consultarFoto.php",
        type: "POST",
        data: formdata,
        async: true,
        processData: false,
        contentType: false,
        success: function (data) {
            alert(data); // alert par avisar de que la foto se ha guardado satisfactoriamente
        }
    });
}

function hasGetUserMedia() {
    // Note: Opera builds are unprefixed.
    return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia || navigator.msGetUserMedia);
}